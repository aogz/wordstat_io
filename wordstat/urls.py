from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wordstat.views.home', name='home'),
    url(r'^sign-up/$', 'accounts.views.sign_up_test_view', name='sign-up'),
    url(r'^accounts/', include('accounts.urls')),
	url(r'^site/', include('sites2check.views')),
    url(r'^imperavi/', include('imperavi.urls')),
    # url(r'^newsletter/', include('newsletter.urls')),
    url(r'^admin/django-ses/', include('django_ses.urls')),
    url(r'^admin/', include(admin.site.urls)),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
