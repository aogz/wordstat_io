#-*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from regions.models import *

from decorators import *

import settings
import os,sys,datetime,random,string



@url(r'^ac/$')
@login_required
def autocomplete_list(request):
    if not request.is_ajax(): raise Http404
    result = u''
    regions = Region.objects.all().filter(title_ru__icontains=request.GET.get('q',''))[0:10]
    for r in regions:
        result += u'%s\n' % r.title_ru
    return HttpResponse(result)