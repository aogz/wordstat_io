#-*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin


# Яндекс Регионы
class Region(models.Model):
	title_ru = models.CharField('Имя RU', max_length=150)
	title_en = models.CharField('Имя EN', max_length=150)
	parent = models.ForeignKey('self', verbose_name='Родительская категория', null=True, blank=True)
	order = models.IntegerField('Порядок')
	def __unicode__(self):
		return self.title_ru
		if self.parent:
			return u'%s (%s)' % (self.title_ru, self.parent.title_ru)
		else:
			return self.title_ru
	class Meta:
		ordering = ('title_ru',) #('order',)
		verbose_name = "Регион"
		verbose_name_plural = "Регионы"


class GoogleRegion(models.Model):
	title_ru = models.CharField('Имя RU', max_length=150)
	code = models.CharField('Буквенный код', max_length=4)
	
	def __unicode__(self):
		return self.title_ru

	class Meta:
		ordering = ('title_ru',)
		verbose_name = "Регион Google"
		verbose_name_plural = "Регионы Google"