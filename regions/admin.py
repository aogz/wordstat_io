#-*- coding: utf-8 -*-

from regions.models import *
from django.contrib import admin

class RegionAdmin(admin.ModelAdmin):
	list_display = ('title_ru',)
	search_fields = ('title_ru', 'title_en')
admin.site.register(Region, RegionAdmin)


admin.site.register(GoogleRegion)