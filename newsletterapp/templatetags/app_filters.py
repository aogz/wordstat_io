from django import template

register = template.Library()

def do_render_content(parser, token):
    split = token.split_contents()
    return RenderContentNode(split[1])


class RenderContentNode(template.Node):
    def __init__(self, content_variable):
        self.content_variable = template.Variable(content_variable)
        
    def render(self, context):
        content_variable = self.content_variable.resolve(context) # we now have template string, which we should render
        return template.Template(content_variable).render(context) # we render template string with given context

register.tag('render_content', do_render_content)