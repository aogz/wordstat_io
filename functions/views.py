# -*- coding: utf8 -*-
# Create your views here.

import settings
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.core.cache import cache
from django.utils.hashcompat import md5_constructor
from django.utils.http import urlquote

import datetime


def get_week(week_from_now=0, day_from_now=0):
	days = []
	now = datetime.datetime.now()
	for nextday in range(7):
		days.append( now + datetime.timedelta(days=nextday+7*week_from_now+day_from_now,) )
	return days


def compose(func_1, func_2, unpack=False):
    """
    compose(func_1, func_2, unpack=False) -> function

    The function returned by compose is a composition of func_1 and func_2.
    That is, compose(func_1, func_2)(5) == func_1(func_2(5))
    """
    if not callable(func_1):
        raise TypeError("First argument to compose must be callable")
    if not callable(func_2):
        raise TypeError("Second argument to compose must be callable")

    if unpack:
        def composition(*args, **kwargs):
            return func_1(*func_2(*args, **kwargs))
    else:
        def composition(*args, **kwargs):
            return func_1(func_2(*args, **kwargs))
    return composition


def invalidate_template_cache(fragment_name, *variables):
    """
    clears template cache block (i.e. {% cache 6000 blabla %})
    """
    args = md5_constructor(u':'.join(apply(compose(urlquote, unicode), variables)))
    cache_key = 'template.cache.%s.%s' % (fragment_name, args.hexdigest())
    cache.delete(cache_key)