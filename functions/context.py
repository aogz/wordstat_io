#-*- coding: utf-8 -*-

from mysite.themes.models import *
from mysite.config.models import *
from mysite.menus.models import *

# load site config
def config(request):
	config = None
	try:
		config = Config.objects.all()[0]
	except:
		pass
	return { 'config':config, }


# load all themes. Default theme goes first!
def themes(request):
	try:
		config = Config.objects.all()[0]
	except:
		return { }
	themes = Theme.objects.all().exclude(id=config.theme.id)
	
	result = []
	result.append(config.theme)
	for t in themes:
		result.append(t)

	return { 'themes': result, }
