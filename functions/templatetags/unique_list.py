from django import template

register = template.Library()

# returns only unique values of list
def unique_list(lst):
	return list( set(lst) ) # remove duplications

register.filter('unique_list', unique_list)
