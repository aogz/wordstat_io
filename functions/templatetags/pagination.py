#-*- coding: utf-8 -*-

# взято отсюда: http://www.djangosnippets.org/snippets/673/

import re

from django import template
from django.db.models.query import QuerySet
from django.core.paginator import Paginator, QuerySetPaginator

register = template.Library()

class PaginateNode(template.Node):
    def __init__(self, objlist_name, nr_pages, var_name="page", param_name="page"):
        self.objlist_name = objlist_name
        self.nr_pages = nr_pages
        self.var_name = var_name
        self.param_name = param_name

    def render(self, context):
        list_or_queryset = template.Variable(self.objlist_name).resolve(context)

        if isinstance(list_or_queryset, QuerySet):
            paginator = QuerySetPaginator(list_or_queryset, self.nr_pages)
        else:
            paginator = Paginator(list_or_queryset, self.nr_pages)

        try:
            page = int(context['request'].REQUEST.get(self.param_name, 1))
        except:
            page = 1

        if page == 0: page = 1

        try:
            context[self.var_name] = paginator.page(page)
            context[self.var_name+"_set"] = paginator
        except:
            return '<pagination error>'

        """
        # MY CODE - insert a new GET variable strings to use instead of calling get_absolute_path method
        path = context['request'].get_full_path()
        if 'page=' in path:
            context[self.var_name+"_prevpagepath"] = re.sub('page=[0-9]+', 'page='+str(paginator.page(page).previous_page_number()), path)
            context[self.var_name+"_nextpagepath"] = re.sub('page=[0-9]+', 'page='+str(paginator.page(page).next_page_number()), path)
        else:
            if path.find('?') != -1:
                context[self.var_name+"_prevpagepath"] = path + '&page=' + str(paginator.page(page).previous_page_number())
                context[self.var_name+"_nextpagepath"] = path + '&page=' + str(paginator.page(page).next_page_number())
            else:
                context[self.var_name+"_prevpagepath"] = path + '?page=' + str(paginator.page(page).previous_page_number())
                context[self.var_name+"_nextpagepath"] = path + '?page=' + str(paginator.page(page).next_page_number())
        """

        # backward compatibility injections into the context
        context[self.var_name+"_count"] = paginator.count
        context[self.var_name+"_num_pages"] = paginator.num_pages
        context[self.var_name+"_page_range"] = paginator.page_range
        return ''

def paginate(parser, token):
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires arguments" % token.contents.split()[0]
    args = arg.split(None)
    var_name = "page"
    param_name = "page"
    try:
        objlist_name = args[0]
        nr_pages = args[1]
    except:
        raise template.TemplateSyntaxError, "%r tag invalid arguments" % token.contents.split()[0]
    if len(args)==2:
        pass
    elif len(args)==4 and args[2]=="as":
        var_name = args[3]
    elif len(args)==6 and args[4]=="using":
        var_name = args[3]
        param_name = args[5]
    else:
        raise template.TemplateSyntaxError, "%r tag invalid arguments" % token.contents.split()[0]
    try:
        nr_pages = int(nr_pages)
    except:
        raise template.TemplateSyntaxError, "%r tag invalid arguments" % token.contents.split()[0]
    return PaginateNode(objlist_name, nr_pages, var_name, param_name)
paginate = register.tag(paginate)




class GetPageNode(template.Node):
    def __init__(self, item, instance):
        self.item = template.Variable(item)
        self.instance = template.Variable(instance)

    def render(self, context):
        self.item = self.item.resolve(context)
        self.instance = self.instance.resolve(context)

        item_index = 0
        divider = self.instance.per_page

        for i in range( len(self.instance.object_list) ):
            if self.instance.object_list[i] == self.item: item_index = i

        page = 1 + int(item_index / divider)

        result = ''

        # insert a new GET variable `page=N` to use instead of calling get_absolute_path method
        path = context['request'].get_full_path()
        if 'page=' in path:
            result = re.sub('page=[0-9]+', 'page='+str(page), path)
        else:
            if path.find('?') != -1:
                result = path + '&page=' + str(page)
            else:
                result = path + '?page=' + str(page)

        return result

def get_page(parser, token):
    """
    {% get_page for ITEM in PAGINATOR_INSTANCE %}
    """
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires arguments" % token.contents.split()[0]
    args = arg.split(None)
    if args[0] != 'for': raise template.TemplateSyntaxError, "%r tag invalid arguments" % token.contents.split()[0]
    if args[2] != 'in': raise template.TemplateSyntaxError, "%r tag invalid arguments" % token.contents.split()[0]

    try:
        item = args[1]
        instance = args[3]
    except:
        raise template.TemplateSyntaxError, "%r tag invalid arguments" % token.contents.split()[0]
    return GetPageNode(item, instance)
paginate = register.tag(get_page)
