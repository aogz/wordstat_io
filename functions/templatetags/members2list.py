from django import template

register = template.Library()


# returns iterable list of members of queryset objects or object, i.e. template "{% for subitems in items.subitems %}"
def members2list(qs, member):
	result = []
	if type(qs) == list:
		for q in qs:
			attr = getattr(q, member)
			result.append(attr)
	else:
		attr = getattr(qs, member)
		result.append(attr)
	return result

register.filter('members2list', members2list)
