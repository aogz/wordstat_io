#-*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from sites2check.models import *
from sites2check.forms import *

from wordstat.decorators import *

import wordstat.settings
import os,sys,datetime,random,string

import json

from include import xlwt
from django.template.defaultfilters import striptags

@url(r'^$')
@render_to('brief_page.html')
@login_required
def brief_page(request):
    sites = request.user.user_sites.all()
    return {'sites':sites,}


@url(r'^add/$')
@render_to('add_site.html')
@login_required
def add_site(request):
    if request.method == 'POST':
        form = SiteForm(data=request.POST)
        if form.is_valid():
            nu_site = form.save(commit=False)
            nu_site.user = request.user
            if nu_site.host.startswith('http://'): nu_site.host = nu_site.host.replace('http://','')
            if nu_site.host.endswith('/'): nu_site.host = nu_site.host[0:-1]
            nu_site.host = nu_site.host.strip()
            nu_site.save()
            return HttpResponseRedirect('/site/%s/' % nu_site.host )
        else:
            return {'form':form,}
    else:
        form = SiteForm()
    return {'form':form,}


@url(r'^(.+)/edit/$')
@render_to('edit_site.html')
@login_required
def edit_site(request, host):
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    if request.method == 'POST':
        form = SiteForm(instance=site, data=request.POST)
        if form.is_valid():
            site = form.save(commit=False)
            site.user = request.user
            if site.host.startswith('http://'): site.host = site.host.replace('http://','')
            if site.host.endswith('/'): site.host = site.host[0:-1]
            site.host = site.host.strip()
            site.save()
            return HttpResponseRedirect('/site/%s/' % site.host )
        else:
            return {'form':form, 'site':site, }
    else:
        form = SiteForm(instance=site)
    return {'form':form, 'site':site, }



@url(r'^(.+)/delete-site/$')
@login_required
def delete_site(request, host):
    if not request.is_ajax(): return HttpResponse('only ajax requests')
    if not request.user.is_authenticated(): return HttpResponse('only authorized users')
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    site.delete()
    return HttpResponse('ok')



@url(r'^(.+)/add-query/$')
@render_to('add_query.html')
@login_required
def add_query(request, host):
    site = get_object_or_404(Site2Check, host=host, user=request.user)

    if request.method == 'POST':
        form = QueryForm(data=request.POST)
        if form.is_valid():
            if site.site_queries.all().count() + 1 > \
                    site.user.profile.account.limit_for_keywords_monitoring:
                return HttpResponse('Too many queries. Try upgrading your account.')
            nu_query = form.save(commit=False)
            nu_query.site = site
            if form.cleaned_data.get('region_str',''):
                nu_query.region = Region.objects.get(title_ru=form.cleaned_data.get('region_str',''))
            else:
                nu_query.region = None
            nu_query.save()
            return HttpResponseRedirect('/site/%s/' % site.host )
        else:
            return {'form':form, 'site':site, }
    else:
        form = QueryForm()
    return {'form':form, 'site':site,}


@url(r'^(.+)/edit-query-bulk/$')
@render_to('edit_query_bulk.html')
@login_required
def edit_query_bulk(request, host):
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    current_queries = site.serializeAllQueries()
    if request.method == 'POST':
        form = EditQueryBulkForm(data=request.POST)
        if form.is_valid():
            site.site_queries.all().delete()
            for q in form.cleaned_data.get('queries','').split('\n'):
                q = q.strip()
                if len(q) < 2: continue
                if site.site_queries.all().count() > \
                        site.user.profile.account.limit_for_keywords_monitoring:
                    return HttpResponse('Too many queries. Try upgrading your account.')
                Query(site=site, query=q).save()

            return HttpResponseRedirect('/site/%s/' % site.host )
        else:
            return {'form':form, 'site':site,}
    else:
        form = EditQueryBulkForm(initial={'queries':current_queries},)
    return {'form':form, 'site':site,}





@url(r'^(.+)/edit-query/([0-9]+)/$')
@render_to('edit_query.html')
@login_required
def edit_query(request, host, query_pk):
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    query = get_object_or_404(Query, pk=query_pk, site=site)
    if request.method == 'POST':
        form = QueryForm(instance=query, data=request.POST)
        if form.is_valid():
            old_query_text = query.query
            old_region = query.region
            query = form.save(commit=False)
            if form.cleaned_data.get('region_str',''):
                query.region = Region.objects.get(title_ru=form.cleaned_data.get('region_str',''))
            else:
                query.region = None
            if form.cleaned_data['query'] != old_query_text or old_region != query.region:
                query.wordstat_popularity = None # reset wordstat popularity if query text has changed
            query.save()
            return HttpResponseRedirect('/site/%s/' % site.host )
        else:
            return {'form':form, 'site':site, 'query':query,}
    else:
        form = QueryForm(instance=query)
    return {'form':form, 'site':site, 'query':query,}


@url(r'^(.+)/delete-query/$')
@login_required
def delete_query(request, host):
    if not request.is_ajax(): return HttpResponse('only ajax requests')
    if not request.user.is_authenticated(): return HttpResponse('only authorized users')
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    query_pk = request.POST.get('query_pk','')
    if not query_pk: return HttpResponse('specify query pk')
    query = get_object_or_404(Query, pk=query_pk, site=site)
    query.delete()
    return HttpResponse('ok')




@url(r'^(.+)/csv/$')
@login_required
def site_positions_view_csv(request, host):
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    csv = render_to_string( 'positions.csv', { 'site': site }, context_instance=RequestContext(request) )
    return HttpResponse(csv, mimetype='text/csv')




@url(r'^(.+)/guest-(.+)/xls/$')
def site_positions_export_xls_guest(request, host, pwd):
    site = get_object_or_404(Site2Check, host=host)
    date_from = request.GET.get('date_from','')
    date_to = request.GET.get('date_to','')

    # check password
    if pwd != site.getHashPass(): return HttpResponse('Неверный пароль')

    # redirect with date parameters (today\yesterday) if needed
    if not date_from or not date_to:
        return HttpResponse('Ошибка: Дата 1 и Дата 2 не выбраны')

    queries = site.site_queries.all()
    queries_list = []

    try:
        date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')
        date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')
    except:
        return HttpResponse(u'Неверный формат дат')


    if date_from > date_to: return HttpResponse('Дата 1 должна быть меньше или равна Дате 2')

    bookname = export_to_xls(site=site, date_from=date_from, date_to=date_to)

    return HttpResponseRedirect(os.path.join(settings.MEDIA_URL, 'site-reports-xls', bookname))



@url(r'^(.+)/xls/$')
@login_required
def site_positions_export_xls(request, host):
    site = get_object_or_404(Site2Check, host=host, user=request.user)
    date_from = request.GET.get('date_from','')
    date_to = request.GET.get('date_to','')

    # redirect with date parameters (today\yesterday) if needed
    if not date_from or not date_to:
        return HttpResponse('Ошибка: Дата 1 и Дата 2 не выбраны')

    queries = site.site_queries.all()
    queries_list = []

    try:
        date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')
        date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')
    except:
        return HttpResponse(u'Неверный формат дат')


    if date_from > date_to: return HttpResponse('Дата 1 должна быть меньше или равна Дате 2')

    bookname = export_to_xls(site=site, date_from=date_from, date_to=date_to)

    return HttpResponseRedirect(os.path.join(settings.MEDIA_URL, 'site-reports-xls', bookname))



@url(r'^add-bulk/$')
@render_to('add_site_bulk.html')
@login_required
def add_site_bulk(request):
    if request.method == 'POST':
        form = SiteBulkForm(data=request.POST)
        if form.is_valid():
            site_list = form.cleaned_data.get('sites','').split('\n')
            for host in site_list:
                if host.startswith('http://'): host = host.replace('http://','')
                if host.startswith('www.'): host = host.replace('www.','')
                if host.endswith('/'): host = host[0:-1]
                host = host.strip()
                if not host: continue
                try:
                    site, is_created = Site2Check.objects.get_or_create(user=request.user, host=host)
                except:
                    return HttpResponse(u'Ошибка добавления сайта. Свяжитесь с администрацией ресурса.')
                if not is_created: continue
                site.save()
            return HttpResponseRedirect('/site/' )
        else:
            return {'form':form,}
    else:
        form = SiteBulkForm()
    return {'form':form,}



@url(r'^(.+)/guest-(.+)/$')
@render_to('positions_guest.html')
def site_positions_view_guest(request, host, pwd):
    start = datetime.datetime.now()

    matching_sites = Site2Check.objects.filter(host=host)
    for matching_site in matching_sites:
        if pwd == matching_site.getHashPass():

            site = matching_site

            date_from = request.GET.get('date_from','')
            date_to = request.GET.get('date_to','')

            # redirect with date parameters (today\yesterday) if needed
            if not date_from or not date_to:
                return HttpResponseRedirect(u'/site/%s/guest-%s/?date_from=%s&date_to=%s' % (site.host, site.getHashPass(), (datetime.datetime.now()-datetime.timedelta(days=1)).strftime('%d/%m/%Y'), datetime.datetime.now().strftime('%d/%m/%Y'),  ))

            queries = site.site_queries.all()
            queries_list = []
            total_days = 0


            try:
                date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')
                date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')
            except:
                return HttpResponse(u'Неверный формат дат')


            if date_from > date_to: return HttpResponse('Дата 1 должна быть меньше или равна Дате 2')

            form = DatesForm(initial={'date_from':date_from, 'date_to':date_to})

            for query in queries:
                queries_list.append( (query, Query.objects.get_positions_dict_for_dates(query, date_from, date_to)) )

            # calculate effective shows
            effective_shows = 0
            cur_date = date_from
            while cur_date <= date_to:
                total_days += 1
                effective_shows += Site2Check.objects.get_shows_for_date(site=site, date=cur_date) # получаем число эффективных показов за определенную дату
                cur_date += datetime.timedelta(days=1)

            # calculate visitors
            visitors = []
            cur_date = date_from
            while cur_date <= date_to:
                visitors.append( (cur_date, Site2Check.objects.get_visitors_for_date(site=site, date=cur_date)) ) # получаем примерное число посетителей за определенную дату
                cur_date = cur_date + datetime.timedelta(days=1)


            # calculate TIC, PR, indexed
            tic = {'from':TIC.objects.get_tic_for_date(site,date_from), 'to':TIC.objects.get_tic_for_date(site,date_to),}
            pr = {'from':PR.objects.get_pr_for_date(site,date_from), 'to':PR.objects.get_pr_for_date(site,date_to),}
            indexed_yandex = {'from':IndexedYandex.objects.get_indexed_for_date(site,date_from), 'to':IndexedYandex.objects.get_indexed_for_date(site,date_to),}
            indexed_google = {'from':IndexedGoogle.objects.get_indexed_for_date(site,date_from), 'to':IndexedGoogle.objects.get_indexed_for_date(site,date_to),}

            gen_time = datetime.datetime.now() - start

            pos_dict = {
                'yandex_from': Position.objects.filter(query=site, searchEngine='Yandex').for_date(date_from),
                'yandex_to' : Position.objects.filter(query=site, searchEngine='Yandex').for_date(date_to),
                'google_from': Position.objects.filter(query=site,searchEngine='Google').for_date(date_from),
                'google_to' : Position.objects.filter(query=site, searchEngine='Google').for_date(date_to)
            }

            return { 'form':form, 'date_from':date_from, 'date_to':date_to,
                     'queries_list':queries_list, "site":site, 'effective_shows':effective_shows,
                     'visitors':visitors, 'tic': tic, 'pr':pr, 'indexed_yandex': indexed_yandex,
                     'indexed_google': indexed_google, 'total_days':total_days,
                     'gen_time':gen_time, 'pos_dict': pos_dict }

    # check password
    return HttpResponse('Неверный URL')



@url(r'^(.+)/$')
@render_to('positions.html')
@login_required
def site_positions_view(request, host):
    start = datetime.datetime.now()

    site = get_object_or_404(Site2Check, host=host, user=request.user)
    date_from = request.GET.get('date_from','')
    date_to = request.GET.get('date_to','')

    # redirect with date parameters (today\yesterday) if needed
    if not date_from or not date_to:
        return HttpResponseRedirect(u'/site/%s/?date_from=%s&date_to=%s' % (site.host, (datetime.datetime.now()-datetime.timedelta(days=1)).strftime('%d/%m/%Y'), datetime.datetime.now().strftime('%d/%m/%Y'),  ))

    queries = site.site_queries.all()
    queries_list = []
    total_days = 0
    
    try:
        date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')
        date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')
    except:
        return HttpResponse(u'Неверный формат дат')


    if date_from > date_to: return HttpResponse('Дата 1 должна быть меньше или равна Дате 2')

    # Chart generating
    if request.is_ajax():
        id = request.GET['chart-id']
        chart_data_google = []
        chart_date_yandex = []
        for x in range(0, (date_to - date_from).days + 1):
            date = (date_from + datetime.timedelta(days=x))
            try:
                g_value = Position.objects.filter(query=Query.objects.get(id=id), 
                    searchEngine='Google').get_latest(date).position
                y_value = Position.objects.filter(query=Query.objects.get(id=id), 
                    searchEngine='Yandex').get_latest(date).position
            except:
                g_value = 0
                y_value = 0

            chart_data_google.append({'date': date.strftime('%Y/%m/%d'), 'value': g_value})
            chart_date_yandex.append({'date': date.strftime('%Y/%m/%d'), 'value': y_value})
            chart_data = [chart_data_google, chart_date_yandex]

        return HttpResponse(json.dumps(chart_data))

    form = DatesForm(initial={'date_from':date_from, 'date_to':date_to})

    # print datetime.datetime.now() - start # DEBUG

    for query in queries:
        queries_list.append( (query, Query.objects.get_positions_dict_for_dates(query, date_from, date_to)) )

    # print datetime.datetime.now() - start # DEBUG

    # calculate effective shows
    effective_shows = 0
    cur_date = date_from
    while cur_date <= date_to:
        total_days += 1
        effective_shows += Site2Check.objects.get_shows_for_date(site=site, date=cur_date) # получаем число эффективных показов за определенную дату
        cur_date += datetime.timedelta(days=1)

    # print datetime.datetime.now() - start # DEBUG

    # calculate visitors
    visitors = []
    cur_date = date_from
    while cur_date <= date_to:
        visitors.append( (cur_date, Site2Check.objects.get_visitors_for_date(site=site, date=cur_date)) ) # получаем примерное число посетителей за определенную дату
        cur_date += datetime.timedelta(days=1)

    # print datetime.datetime.now() - start # DEBUG


    # calculate TIC, PR, indexed
    tic = {'from':TIC.objects.get_tic_for_date(site,date_from), 'to':TIC.objects.get_tic_for_date(site,date_to),}
    pr = {'from':PR.objects.get_pr_for_date(site,date_from), 'to':PR.objects.get_pr_for_date(site,date_to),}
    indexed_yandex = {'from':IndexedYandex.objects.get_indexed_for_date(site,date_from), 'to':IndexedYandex.objects.get_indexed_for_date(site,date_to),}
    indexed_google = {'from':IndexedGoogle.objects.get_indexed_for_date(site,date_from), 'to':IndexedGoogle.objects.get_indexed_for_date(site,date_to),}

    gen_time = datetime.datetime.now() - start # DEBUG


    # Get Yandex and Google Positions
    pos_dict = {
        'yandex_from': Position.objects.filter(query=site, searchEngine='Yandex').for_date(date_from),
        'yandex_to' : Position.objects.filter(query=site, searchEngine='Yandex').for_date(date_to),
        'google_from': Position.objects.filter(query=site, searchEngine='Google').for_date(date_from),
        'google_to' : Position.objects.filter(query=site, searchEngine='Google').for_date(date_to)
    }


    return { 'form':form, 'date_from':date_from, 'date_to':date_to, 'queries_list':queries_list,
             "site":site, 'effective_shows':effective_shows, 'visitors':visitors, 'tic': tic,
             'pr':pr, 'indexed_yandex': indexed_yandex, 'indexed_google': indexed_google,
             'total_days':total_days, 'gen_time':gen_time, 'pos_dict' : pos_dict}