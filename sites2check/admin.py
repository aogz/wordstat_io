#-*- coding: utf-8 -*-

from django.contrib import admin
from sites2check.models import *

"""
class NewsAdmin(admin.ModelAdmin):
	fields = ('headline', 'text', 'frontpage', 'is_published', )
	list_display = ('headline', 'date_added', 'frontpage', 'author', 'is_published',)
	list_filter = ('headline', 'date_added', 'author', 'is_published')
	ordering = ('-date_added',)
	search_fields = ('headline', 'text', 'date_added',)

	class Media:
		js = ('tiny_mce/tiny_mce.js',
				'tiny_mce/textareas.js',)
admin.site.register(News, NewsAdmin)
"""

class Site2CheckAdmin(admin.ModelAdmin):
    list_display = ('user', 'host', 'created', 'guest_url', 'checked')
    list_filter = ('user',)
    search_fields = ('host', 'user__username',)
admin.site.register(Site2Check, Site2CheckAdmin)

class QueryAdmin(admin.ModelAdmin):
	list_display = ('site', 'query', 'region', 'google_region', 'wordstat_popularity',)
	list_filter = ('site',)
	search_fields = ('site__host', 'query',)
admin.site.register(Query, QueryAdmin)

class PositionAdmin(admin.ModelAdmin):
	list_display = ('created', 'searchEngine', 'position', 'query')

admin.site.register(IndexedYandex)
admin.site.register(IndexedGoogle)

admin.site.register(SiteBacklinks)
admin.site.register(Position, PositionAdmin)
admin.site.register(TIC)
admin.site.register(PR)
