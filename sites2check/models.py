# -*- coding: utf-8 -*-

from django.db import models
from django.db.models.query import QuerySet

from django.contrib.auth.models import User

from wordstat.settings import *

import datetime, hashlib, random, sys, os, string
from include import xlwt

# Create your models here.

_POSITIONS_FOR_DATE = {}

class QuerySetManager(models.Manager):
    use_for_related_fields = True
    def __init__(self, qs_class=models.query.QuerySet):
        self.queryset_class = qs_class
        super(QuerySetManager, self).__init__()

    def get_query_set(self):
        return self.queryset_class(self.model)

    def __getattr__(self, attr, *args):
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            return getattr(self.get_query_set(), attr, *args)




def _xls_format_positions(pos_from, pos_to):
    if pos_from != None:
        pos_from = pos_from.position
    else:
        pos_from = ''
    if pos_to != None:
        pos_to = pos_to.position
    else:
        pos_to = ''
    return u'%s → %s' % (pos_from, pos_to)


def _xls_format_delta(pos_from, pos_to):
    style_red = xlwt.easyxf('font: color-index red; align: vert centre, horiz center')
    style_green = xlwt.easyxf('font: color-index green, bold on; align: vert centre, horiz center')
    style_default = xlwt.easyxf('align: vert centre, horiz center')

    delta_str = ''
    current_style = style_default
    if pos_from and pos_to:
        if pos_to.position > pos_from.position:
            current_style = style_red
            delta_str = u'+%i' % (pos_to.position - pos_from.position)
        elif pos_to.position < pos_from.position:
            current_style = style_green
            delta_str = u'%i' % (pos_to.position - pos_from.position)
        return {'delta': delta_str, 'style': current_style}
    return None


def _random_string(len=6):
    result = ''
    for i in range(len):
        result += random.choice(string.letters)
    return result


def _get_first_or_none(items):
    if len(items):
        return items[0]
    else:
        return None


def export_to_xls(site, date_from, date_to):
    queries = site.site_queries.all()
    queries_list = []

    for query in queries:
        queries_list.append(Query.objects.get_positions_dict_for_dates(query, date_from, date_to))

    style_heading = xlwt.easyxf('font: bold on; align: vert centre, horiz center')
    book = xlwt.Workbook(encoding="utf-8")
    sheet1 = book.add_sheet(u"%i.%i.%i→%i.%i.%i" % (
    date_from.day, date_from.month, date_from.year, date_to.day, date_to.month, date_to.year, ))
    sheet1.write(0, 0, 'Запрос', style_heading)
    sheet1.write(0, 1, 'Геотаргетинг', style_heading)
    sheet1.write(0, 2, 'Популярность (Wordstat)', style_heading)
    sheet1.write(0, 3, 'Позиция в Яндексе', style_heading)
    sheet1.write(0, 4, 'Динамика по Яндексу', style_heading)
    sheet1.write(0, 5, 'Позиция в Google', style_heading)
    sheet1.write(0, 6, 'Динамика по Google', style_heading)

    index_query = 1
    for query_dict in queries_list:
        if query_dict['query'].hilite_color:
            sheet1.write(index_query, 0, query_dict['query'].query, xlwt.easyxf(
                'font: color-index %s' % query_dict['query'].hilite_color))  # Запрос
        else:
            sheet1.write(index_query, 0, query_dict['query'].query)

        if query_dict['query'].region and query_dict['query'].region.pk != 1: sheet1.write(
            index_query, 1, query_dict['query'].region.title_ru.encode('utf8'))
        if query_dict['query'].wordstat_popularity: sheet1.write(index_query, 2, query_dict[
            'query'].wordstat_popularity)

        sheet1.write(index_query, 3, _xls_format_positions(query_dict['yandex_from'], query_dict[
            'yandex_to']))  # Позиция в Яндексе

        delta_dict = _xls_format_delta(query_dict['yandex_from'], query_dict['yandex_to'])
        if delta_dict: sheet1.write(index_query, 4, delta_dict['delta'],
                                    delta_dict['style'])  # Динамика по Яндексу

        sheet1.write(index_query, 5, _xls_format_positions(query_dict['google_from'], query_dict[
            'google_to']))  # Позиция в Google

        delta_dict = _xls_format_delta(query_dict['google_from'], query_dict['google_to'])
        if delta_dict: sheet1.write(index_query, 6, delta_dict['delta'],
                                    delta_dict['style'])  # Динамика по Google

        index_query += 1

    if not os.path.exists(os.path.join(MEDIA_ROOT, 'site-reports-xls')): os.mkdir(
        os.path.join(MEDIA_ROOT, 'site-reports-xls'))

    bookname = '%s_%s.xls' % (site.host, _random_string())
    bookpath = os.path.join(MEDIA_ROOT, 'site-reports-xls', bookname)

    if os.path.exists(bookpath): os.remove(bookpath)
    book.save(bookpath)

    return bookname



class SiteManager(models.Manager):
    def get_shows_for_date(self, site, date):
        """
        возвращает количество эффективных показов (топ 10) за указанный день
        """
        allshows = 0
        queries = site.site_queries.all().filter(wordstat_popularity__isnull=False).filter(
            wordstat_popularity__gt=0).distinct()
        for query in queries:
            query_top_ya_position = query.get_positions().for_date( date).filter(
                searchEngine='Yandex').first()  # правильнее было добавить .filter(position__lte=10) (т.е. фильтровать только позиции из топ 10), но это сцуко тогда жутко тормозит
            query_top_go_position = query.get_positions().for_date( date).filter(
                searchEngine='Google').first()

            if query_top_ya_position: query_top_ya_position = query_top_ya_position.position
            if query_top_ya_position: query_top_go_position = query_top_go_position.position

            # оцениваем эффективные показы по яндексу
            if query_top_ya_position <= 3:
                allshows += query.wordstat_popularity / 30
            elif query_top_ya_position == 4:
                allshows += query.wordstat_popularity / 30 * 0.85
            elif query_top_ya_position == 5:
                allshows += query.wordstat_popularity / 30 * 0.6
            elif query_top_ya_position == 6 or query_top_ya_position == 7:
                allshows += query.wordstat_popularity / 30 * 0.5
            elif query_top_ya_position == 8 or query_top_ya_position == 9:
                allshows += query.wordstat_popularity / 30 * 0.3
            elif query_top_ya_position == 10:
                allshows += query.wordstat_popularity / 30 * 0.2

            # оцениваем эффективные показы по Google
            google_rank = 0.4  # процент людей, которые пользуются Google (по отношению к Яндексу)
            if query_top_go_position <= 3:
                allshows += query.wordstat_popularity / 30 * google_rank
            elif query_top_go_position == 4:
                allshows += query.wordstat_popularity / 30 * 0.85 * google_rank
            elif query_top_go_position == 5:
                allshows += query.wordstat_popularity / 30 * 0.6 * google_rank
            elif query_top_go_position == 6 or query_top_go_position == 7:
                allshows += query.wordstat_popularity / 30 * 0.5 * google_rank
            elif query_top_go_position == 8 or query_top_go_position == 9:
                allshows += query.wordstat_popularity / 30 * 0.3 * google_rank
            elif query_top_go_position == 10:
                allshows += query.wordstat_popularity / 30 * 0.2 * google_rank

        return int(allshows)


    def get_visitors_for_date(self, site, date):
        """
        возвращает примерное число посетителей за указанный день
        """
        allvisitors = 0
        """
        REFACTORING NEEDED!
        queries = site.site_queries.all().filter(wordstat_popularity__isnull=False).filter(wordstat_popularity__gt=0)
        for query in queries:
            query_top_ya_position = query.get_positions.filter(searchEngine='Yandex').for_date( date).first()
            query_top_go_position = query.get_positions.filter(searchEngine='Google').for_date( date).first()

            if query_top_ya_position:
                if query_top_ya_position.position <= 3: allvisitors += query.wordstat_popularity / 30 * 0.5
                elif query_top_ya_position.position <= 7: allvisitors += query.wordstat_popularity / 30 * 0.3
                elif query_top_ya_position.position <= 10: allvisitors += query.wordstat_popularity / 30 * 0.1
                elif query_top_ya_position.position <= 20: allvisitors += query.wordstat_popularity / 30 * 0.05

            if query_top_go_position:
                if query_top_go_position.position <= 3: allvisitors += query.wordstat_popularity / 30 * 0.5 * 0.2
                elif query_top_go_position.position <= 7: allvisitors += query.wordstat_popularity / 30 * 0.3 * 0.2
                elif query_top_go_position.position <= 10: allvisitors += query.wordstat_popularity / 30 * 0.1 * 0.2
                elif query_top_go_position.position <= 20: allvisitors += query.wordstat_popularity / 30 * 0.05 * 0.2
        """
        return int(allvisitors)


class Site2Check(models.Model):
    objects = SiteManager()

    user = models.ForeignKey(User, verbose_name='Пользователь', related_name='user_sites')
    host = models.CharField('Хост', help_text='Например, ya.ru, google.com и т.п', max_length=150)
    created = models.DateTimeField('Добавлен', auto_now_add=True)

    checked = models.DateTimeField('Дата последней проверки', blank=True, null=True)

    def get_absolute_url(self):
        return u'/site/%s/' % self.host

    def __unicode__(self):
        return u'%s' % self.host

    def serializeAllQueries(self):
        result = u""
        queries = self.site_queries.all()
        for q in queries:
            result += u"%s\n" % q.query.strip()
        return result

    def getHashPass(self):
        h = hashlib.md5()
        h.update('%s - %s - Ldsk439KKnbna1' % (self.host, self.user.username))
        return h.hexdigest()[0:6]

    def guest_url(self):
        return u"<a href='/site/%s/guest-%s/' target='_blank'>Перейти</a>" % (
        self.host, self.getHashPass())

    guest_url.short_description = 'Гостевой URL'
    guest_url.allow_tags = True

    def to_check(self):
        """ returns True if this site should be checked; False otherwise (inactive, balance < 0 etc) """
        if self.user.get_profile().balance <= 0:
            return False
        return True

    def is_se_info_parsed_today(self):
        if self.site_tics.filter(created__day=datetime.datetime.now().day,
                                 created__month=datetime.datetime.now().month,
                                 created__year=datetime.datetime.now().year).count() and self.site_prs.filter(
                created__day=datetime.datetime.now().day,
                created__month=datetime.datetime.now().month,
                created__year=datetime.datetime.now().year).count() and self.site_indexes_yandex.filter(
                created__day=datetime.datetime.now().day,
                created__month=datetime.datetime.now().month,
                created__year=datetime.datetime.now().year).count() and self.site_indexes_google.filter(
                created__day=datetime.datetime.now().day,
                created__month=datetime.datetime.now().month,
                created__year=datetime.datetime.now().year).count():
            return True
        else:
            return False

    def get_down_queries_yandex(self):
        """
        возвращает запросы, по которым было падение позиций в Яндексе
        """
        today = datetime.datetime.now()
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        down_queries = []
        for sq in self.site_queries.all():
            d = Query.objects.get_positions_dict_for_dates(sq, yesterday, today)
            if (d['yandex_from'] and d['yandex_to']) and (
                d['yandex_to'].position > d['yandex_from'].position):
                down_queries.append(sq)
        return down_queries

    def get_down_queries_google(self):
        """
        возвращает запросы, по которым было падение позиций в Google
        """
        today = datetime.datetime.now()
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        down_queries = []
        for sq in self.site_queries.all():
            d = Query.objects.get_positions_dict_for_dates(sq, yesterday, today)
            if (d['google_from'] and d['google_to']) and (
                d['google_to'].position > d['google_from'].position):
                down_queries.append(sq)
        return down_queries

    def get_up_queries_yandex(self):
        """
        возвращает запросы, по которым был рост позиций в Яндексе
        """
        today = datetime.datetime.now()
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        up_queries = []
        for sq in self.site_queries.all():
            d = Query.objects.get_positions_dict_for_dates(sq, yesterday, today)
            if (d['yandex_from'] and d['yandex_to']) and (
                d['yandex_to'].position < d['yandex_from'].position):
                up_queries.append(sq)
        return up_queries

    def get_up_queries_google(self):
        """
        возвращает запросы, по которым был рост позиций в Google
        """
        today = datetime.datetime.now()
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        up_queries = []
        for sq in self.site_queries.all():
            d = Query.objects.get_positions_dict_for_dates(sq, yesterday, today)
            if (d['google_from'] and d['google_to']) and (
                d['google_to'].position < d['google_from'].position):
                up_queries.append(sq)
        return up_queries

    class Meta:
        ordering = ('host',)
        verbose_name = 'Сайт на проверку'
        verbose_name_plural = 'Сайты на проверку'


class QueryManager(models.Manager):
    def get_positions_dict_for_dates(self, query, date_from, date_to):
        """
        возвращает словарь с позициями на даты date_from и date_to
        positions_list - необязательный параметр - QuerySet отфильтрованных позиций MongoPosition
        кэширует запросы для конкретной даты в словарь _POSITIONS_FOR_DATE
        """
        # MONGO DB
        queries_dict = {}
        position_yandex_from = query.get_positions().filter(searchEngine='Yandex').for_date(date_from).first()  # it would be rather correct to add 'order_by' for last position, but 'order_by' is damn sloooow!
        position_yandex_to = query.get_positions().filter(searchEngine='Yandex').for_date(date_to).first()
        position_google_from = query.get_positions().filter(searchEngine='Google').for_date(date_from).first()
        position_google_to = query.get_positions().filter(searchEngine='Google').for_date(date_to).first()
        queries_dict.update(
            {'query': query, 'yandex_from': position_yandex_from, 'yandex_to': position_yandex_to,
             'google_from': position_google_from, 'google_to': position_google_to})

        return queries_dict

    """
    def get_positions_dict_latest(self, query):
        queries_dict = {}
        position_yandex_from = _get_first_or_none( query.get_positions_for_yandex() )
        if position_yandex_from: position_yandex_from = position_yandex_from.get_before_this()

        position_google_from = _get_first_or_none( query.get_positions_for_google() )
        if position_google_from: position_google_from = position_google_from.get_before_this()

        position_yandex_to = query.get_positions_for_yandex()
        position_google_to = query.get_positions_for_google()

        queries_dict.update( {'query':query, 'google_from':position_google_from, 'google_to':position_google_to, 'yandex_from': position_yandex_from, 'yandex_to': position_yandex_to, } )
        return queries_dict
    """


class Query(models.Model):
    objects = QueryManager()
    site = models.ForeignKey(Site2Check, verbose_name='Сайт', related_name='site_queries')
    query = models.CharField('Запрос', max_length=250)
    region = models.ForeignKey('regions.Region', verbose_name='Регион по Яндексу', blank=True,
                               null=True)
    google_region = models.ForeignKey('regions.GoogleRegion', verbose_name='Регион по Google',
                                      help_text='Выберите регион для Google (необязательно)',
                                      blank=True, null=True)
    wordstat_popularity = models.IntegerField('Популярность по Яндекс Wordstat', blank=True,
                                              null=True)
    hilite_color = models.CharField('Выделение цветом', max_length=30, blank=True, null=True,
                                    help_text='Выделение запроса в списке цветом, например red, green, blue, orange (необязательно)')

    def __unicode__(self):
        return u'%s (сайт %s)' % (self.query, self.site.host)

    def get_positions(self):
        return Position.objects.filter(query=self)

    def if_positions_for_today(self):
        today = datetime.date.today()
        if self.get_positions().filter(searchEngine='Yandex').for_date(today).count() \
                and self.get_positions().filter(searchEngine='Google').for_date(today).count():
            return True
        else:
            return False

    def get_competitors_average_backlinks(self):
        """
        возвращает среднее число бэклинков на сайты-конкуренты из ТОПа Яндекса (сейчас это топ 10)
        """
        total_counted = 10
        all_backlinks = 0
        for query_top_site in QueryTopSites.objects.filter(query=self,
                                                                position__lte=total_counted):
            all_backlinks += query_top_site.backlinks

        return int(float(all_backlinks) / float(total_counted))

    class Meta:
        ordering = ('-wordstat_popularity',)
        verbose_name = 'Запрос'
        verbose_name_plural = 'Запросы'


class SiteBacklinks(models.Model):
    """
    число бэклинков для конкретного сайта
    """
    site = models.OneToOneField(Site2Check, related_name='site_backlinks')
    backlinks = models.IntegerField('Число бэклинков', blank=True, null=True)

    class Meta:
        verbose_name = 'Бэклинк сайта'
        verbose_name_plural = 'Бэклинки сайта'


class TICManager(models.Manager):
    def get_tic_for_date(self, site, date):
        return _get_first_or_none(TIC.objects.filter(site=site, created__day=date.day,
                                                     created__month=date.month,
                                                     created__year=date.year))


class TIC(models.Model):
    objects = TICManager()
    site = models.ForeignKey(Site2Check, verbose_name='Сайт', related_name='site_tics')
    tic = models.IntegerField('Яндекс ТИЦ', blank=True, null=True)
    created = models.DateTimeField('Время проверки', auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.tic

    def show_delta(self):
        previous = self.site.site_tics.filter(created__lt=self.created)
        if previous.count():
            previous = previous[0]
        else:
            return u'---'
        try:
            delta = self.tic - previous.tic
        except:
            return ''
        if delta == 0:
            return delta
        elif delta < 0:
            return u'<span class="moving_down">%s</span>' % delta
        if delta > 0: return u'<span class="moving_up">+%s</span>' % delta

    class Meta:
        ordering = ('-created',)
        verbose_name = verbose_name_plural = 'ТИЦ'


class PRManager(models.Manager):
    def get_pr_for_date(self, site, date):
        return _get_first_or_none(PR.objects.filter(site=site, created__day=date.day,
                                                    created__month=date.month,
                                                    created__year=date.year))


class PR(models.Model):
    objects = PRManager()
    site = models.ForeignKey(Site2Check, verbose_name='Сайт', related_name='site_prs')
    pr = models.IntegerField('Google PR', blank=True, null=True)
    created = models.DateTimeField('Время проверки', auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.pr

    def show_delta(self):
        previous = self.site.site_prs.filter(created__lt=self.created)
        if previous.count():
            previous = previous[0]
        else:
            return u'---'
        try:
            delta = self.pr - previous.pr
        except:
            return ''
        if delta == 0:
            return delta
        elif delta < 0:
            return u'<span class="moving_down">%s</span>' % delta
        if delta > 0: return u'<span class="moving_up">+%s</span>' % delta

    class Meta:
        ordering = ('-created',)
        verbose_name = verbose_name_plural = 'PR'


class IndexedYandexManager(models.Manager):
    def get_indexed_for_date(self, site, date):
        return _get_first_or_none(IndexedYandex.objects.filter(site=site, created__day=date.day,
                                                               created__month=date.month,
                                                               created__year=date.year))


class IndexedYandex(models.Model):
    objects = IndexedYandexManager()
    site = models.ForeignKey(Site2Check, verbose_name='Сайт', related_name='site_indexes_yandex')
    indexed = models.IntegerField('Индексация в Яндексе', blank=True, null=True)
    created = models.DateTimeField('Время проверки', auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.indexed

    def show_delta(self):
        previous = self.site.site_indexes_yandex.filter(created__lt=self.created)
        if previous.count():
            previous = previous[0]
        else:
            return u'---'
        try:
            delta = self.indexed - previous.indexed
        except:
            return ''
        if delta == 0:
            return delta
        elif delta < 0:
            return u'<span class="moving_down">%s</span>' % delta
        if delta > 0: return u'<span class="moving_up">+%s</span>' % delta

    class Meta:
        ordering = ('-created',)
        verbose_name = verbose_name_plural = 'Индексация в Яндексе'


class IndexedGoogleManager(models.Manager):
    def get_indexed_for_date(self, site, date):
        return _get_first_or_none(IndexedGoogle.objects.filter(site=site, created__day=date.day,
                                                               created__month=date.month,
                                                               created__year=date.year))


class IndexedGoogle(models.Model):
    objects = IndexedGoogleManager()
    site = models.ForeignKey(Site2Check, verbose_name='Сайт', related_name='site_indexes_google')
    indexed = models.IntegerField('Индексация в Google', blank=True, null=True)
    created = models.DateTimeField('Время проверки', auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.indexed

    def show_delta(self):
        previous = self.site.site_indexes_google.filter(created__lt=self.created)
        if previous.count():
            previous = previous[0]
        else:
            return u'---'
        try:
            delta = self.indexed - previous.indexed
        except:
            return ''
        if delta == 0:
            return delta
        elif delta < 0:
            return u'<span class="moving_down">%s</span>' % delta
        if delta > 0: return u'<span class="moving_up">+%s</span>' % delta

    class Meta:
        ordering = ('-created',)
        verbose_name = verbose_name_plural = 'Индексация в Google'


### New models converted from MongoDB ###

ENGINES = (
        ('Yandex', ('Yandex')),
        ('Google', ('Google')),
    )

class PositionManager(models.query.QuerySet):
    def get_latest(self, date, *args, **kwargs):
        start = datetime.datetime(date.year, date.month, date.day)
        end   = start + datetime.timedelta(days=1)
        try:
           return self.filter(created__gte=start, created__lt=end)[:1].get()
        except:
            return self.filter(created__gte=start, created__lt=end)

    def for_date(self, date, *args, **kwargs):
        start = datetime.datetime(date.year, date.month, date.day)
        end   = start + datetime.timedelta(days=1)
        return self.filter(created__gte=start, created__lt=end)

class Position(models.Model):
    searchEngine = models.CharField(max_length=56, choices=ENGINES, help_text='Поисковая система')
    query = models.ForeignKey(Query, help_text='Запрос')
    position = models.IntegerField('Позиция', blank=True, null=True)
    created = models.DateTimeField('Добавлен', auto_now_add=True)
    objects = QuerySetManager(PositionManager)



class QueryTopSites(models.Model):
    query = models.ForeignKey(Query, help_text='Запрос')
    position = models.IntegerField('Позиция', blank=True, null=True)
    host = models.CharField(max_length=255, help_text='Хост сайта, например ya.ru, google.com, и тд')
    backlinks = models.IntegerField('Число бэклинков', blank=True, null=True)


