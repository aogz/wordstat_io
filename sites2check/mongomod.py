#-*- coding: utf-8 -*-

import datetime
from mongoengine import *
from mongoengine.queryset import QuerySet, QuerySetManager



def for_date(self, fieldname, date):
    """
    фильтруем объекты QuerySet для конкретной даты
    добавляем функцию for_date к классу, например SomeClass.objects.filter().for_date('created', datetime.datetime.now())
    """
    start = datetime.datetime(date.year, date.month, date.day)
    end   = start + datetime.timedelta(days=1)
    return self.filter( **{fieldname+'__gte':start, fieldname+'__lt':end} )

setattr(QuerySet, 'for_date', for_date)


def get_latest(self, fieldname, date):
    """
    Получаем последний запрос на текущую дату
    """
    start = datetime.datetime(date.year, date.month, date.day)
    end   = start + datetime.timedelta(days=1)
    try:
        return self.filter( **{fieldname+'__gte':start, fieldname+'__lt':end} )[:1].get()
    except:
        return self.filter( **{fieldname+'__gte':start, fieldname+'__lt':end} )

setattr(QuerySet, 'get_latest', get_latest)



# MONGO DB
class MongoPosition(Document):
    """
    Позиция сайта по конкретному запросу в конкретной поисковой системе
    """
    SE = (
        ('Google', 'Google'),
        ('Yandex', 'Yandex')
    )
    searchEngine = StringField() # Yandex or Google
    query_pk = IntField() # pk of Query (Django) model in models.py
    position = IntField() # integer - position
    created = DateTimeField(default=datetime.datetime.now())

    @property
    def query(self):
        from sites2check.models import Query
        return Query.objects.get(pk=self.query)
    
    class Meta:
        indexes = ('searchEngine', 'quer_ypk', 'created') # определяем индексы
        
# MongoPosition.objects.ensure_index( ['searchEngine','query_pk', 'created'] ) # создаем индексы
    
    
class MongoQueryTopSites(Document):
    """
    Сайты, найденные в ТОПе Яндексе по конкретному запросу 
    """
    query_pk = IntField() # pk of Query (Django) model in models.py
    position = IntField() # позиция, на которой найден сайт
    host = StringField() # домен сайта
    backlinks = IntField() # число бэклинков
    
    