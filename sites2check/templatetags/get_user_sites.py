#-*- coding: utf-8 -*-

from django import template
from sites2check.models import *

register = template.Library()

class RenderNode(template.Node):
    def __init__(self, user, context_var):
        self.user = user
        self.context_var = context_var

    def render(self, context):
        self.user = template.Variable(self.user).resolve(context)
        sites = Site2Check.objects.filter(user=self.user).order_by('host')
        context[self.context_var] = sites
        return ''


def get_user_sites(parser, token):
    """
    {% get_user_sites for USER as VAR %}
    """
    bits = token.contents.split()
 
    return RenderNode(bits[2], bits[4])

get_user_sites = register.tag(get_user_sites)
