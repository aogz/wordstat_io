#-*- coding: utf-8 -*-

from django import template
from sites2check.models import *

register = template.Library()

class RenderNode(template.Node):
    def __init__(self, typ, context_var):
        self.typ = typ
        self.context_var = context_var

    def render(self, context):
        self.typ = template.Variable(self.typ).resolve(context)
        if self.typ == 'users': context[self.context_var] = User.objects.all().count()
        elif self.typ == 'sites': context[self.context_var] = Site2Check.objects.all().count()
        elif self.typ == 'queries': context[self.context_var] = Query.objects.all().count()
        else: return '<invalid type specified>'
        return ''


@register.tag
def get_stats(parser, token):
    """
    {% get_stats [users|sites|queries] as VAR %}
    """
    bits = token.contents.split()
 
    return RenderNode(bits[1], bits[3])
