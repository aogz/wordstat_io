# -*- coding: utf-8 -*-
from __future__ import with_statement
import urllib2, random, os


class CheckError(Exception):
    """
    Базовый класс для исключений GooglePagerank
    """

class IpError(Exception):
    """
    Вызывается, если google.com забанил ip за слишком частые обращения.
    """


class PagerankResult:
    def __init__(self, url, pr):
        self.url = url
        self.pr  = pr

    def __str__(self):
        return 'Google Pagerank for url %s: %s' % (self.url, self.pr)

class GooglePagerank:
    GOOGLE_URL = "http://toolbarqueries.google.com/search?client=navclient-auto&ch=%s&features=Rank&q=info:%s"
    
    def __init__(self, url):
        self.url = url
        
        
    def get_pagerank(self):
        pagerank_url = self._get_url()
        headers = {'User-Agent' : self._get_user_agent(),
                   'Cookie' : ''}
        request = urllib2.Request(url = pagerank_url, headers = headers)
        try:
            conn = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            if e.code == 403:
                html = e.read()
                if html.find('Your client does not have permission', 0) > -1:
                    raise CheckError, "Invalid url (%s)" % (self.url)
                else:
                    raise IpError, "IP Error: Your ip banned"
            else:
	              raise CheckError, "HTTP Error %d: %s" % (e.code, e.msg)
        except urllib2.URLError, e:
	          raise CheckError, "URL Error: %s" % (e.reason.args[1])
        else:
            responce = conn.read()
            if len(responce) == 0: 
                return  PagerankResult(self.url, 'n/a')
            elif responce.find('Rank_', 0) != -1:
                return  PagerankResult(self.url, responce.strip()[-1])
        
    def _get_url(self):
        ch = self._getch(self.url)
        return GooglePagerank.GOOGLE_URL % (ch, self.url)
    
    def _get_user_agent(self):
        with open( os.path.join(os.path.dirname(__file__),"agents.txt") ) as f:
            agents = f.readlines()
        return random.choice(agents).strip()
        
    def _getch(self, string):
        return self._check_hash(self._hash_url(string))
    
    
    def _hash_url(self, string):
        check1 = self._str_to_num(string, 0x1505, 0x21)
        check2 = self._str_to_num(string, 0, 0x1003F)
    
        check1 >>= 2
        check1 = ((check1 >> 4) & 0x3FFFFC0 ) | (check1 & 0x3F)
        check1 = ((check1 >> 4) & 0x3FFC00 ) | (check1 & 0x3FF)
        check1 = ((check1 >> 4) & 0x3C000 ) | (check1 & 0x3FFF) 

        t1 = ((((check1 & 0x3C0) << 4) | (check1 & 0x3C)) <<2 ) | (check2 & 0xF0F )
        t2 = ((((check1 & 0xFFFFC000) << 4) | (check1 & 0x3C00)) << 0xA) | (check2 & 0xF0F0000 )

        return (t1 | t2)
    
    
    def _str_to_num(self, string, check, magic):
       int32unit = 4294967296
       length = len(string)
       for i in range(length):
           check *= magic
           if check >= int32unit:
               check = (check - int32unit * (int) (check / int32unit))
               if check < -2147483648 : check = check + int32unit
               else: check = check
       
           check += ord(string[i]) 
       return check
   
   
    def _check_hash(self, hashnum):
       check_byte = 0
       flag = 0

       hash_str = '%u' % hashnum
       length = len(hash_str)

       for i in reversed(range(length)):
           re = hash_str[i]
           if 1 == (flag % 2):
               re = int(re)       
               re += re 
               re = int((int(re) / 10) + (int(re) % 10))
           

           check_byte += int(re)
           flag += 1

       check_byte %= 10
       if 0 != check_byte:
           check_byte = 10 - check_byte
           if 1 == (flag % 2):
               if 1 == (check_byte % 2):
                   check_byte += 9
               check_byte >>= 1

       return '7' + str(check_byte) + str(hash_str)
