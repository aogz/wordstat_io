#-*- coding: utf-8 -*-
import os, sys, urllib, urllib2, socket, re, time, datetime, codecs, random, logging
from xml.sax.saxutils import escape
from xml.etree import ElementTree
from BeautifulSoup import BeautifulSoup

from urlparse import urlparse
from xgoogle.search import GoogleSearch, SearchError
from xgoogle.pagerank import GooglePagerank, CheckError, IpError


YAHOO_APPID = 'YwyupJ7V34HnPM7L6QVPUx_lQpPv9bFLwIRVukqlbQNI4V0ZCqXK8FG9irMldJDagi_WtQ--'

YANDEX_PATH = '/xmlsearch?user=andnozhkin&key=03.61675979:54eca0786d6ae54416a56ce4dc0296f5'


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        try: self.log = open("/var/log/monitoring.log", "a") # if can write there...
        except: self.log = open( os.path.join(os.path.dirname(__file__),"monitoring.log"), "a") # or write in the same directory
        self.log.write('\n\n# %i.%i.%i\n' %(datetime.datetime.now().day, datetime.datetime.now().month, datetime.datetime.now().year) )

    def write(self, message):
        try:
            self.terminal.write(message)
            self.log.write(message)  
        except:
            self.terminal.write(message.encode('utf8'))
            self.log.write(message.encode('utf8'))  

sys.stdout = Logger() # quick-n-dirty solution to redirect all 'prints' into log file


BROWSERS = [
    # Top most popular browsers in my access.log on 2009.02.12
    # tail -50000 access.log |
    #  awk -F\" '{B[$6]++} END { for (b in B) { print B[b] ": " b } }' |
    #  sort -rn |
    #  head -20
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-ru; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6',
    'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ru-ru; rv:1.9.0.6) Gecko/2009011912 Firefox/3.0.6',
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-ru; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)',
    'Mozilla/5.0 (X11; U; Linux i686; ru-ru; rv:1.9.0.6) Gecko/2009020911 Ubuntu/8.10 (intrepid) Firefox/3.0.6',
    'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru-ru; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6',
    'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru-ru; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)',
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-ru) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.48 Safari/525.19',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',
    'Mozilla/5.0 (X11; U; Linux x86_64; ru-ru; rv:1.9.0.6) Gecko/2009020911 Ubuntu/8.10 (intrepid) Firefox/3.0.6',
    'Mozilla/5.0 (X11; U; Linux i686; ru-ru; rv:1.9.0.5) Gecko/2008121621 Ubuntu/8.04 (hardy) Firefox/3.0.5',
    'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; ru-ru) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/3.2.1 Safari/525.27.1',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
]


PROXIES = ['http://teraprice.ru:12345', 'http://95.170.181.122:12345', 'http://idkfa.org.ru:12345', None] # none for localhost


def cp1251_utf8(txt):
            try:
                return txt.decode('cp1251').encode('utf-8')
            except:
                return txt
            
            
def _post(host, path, data, type=None, use_proxies=True):
    global PROXIES
    
    socket.setdefaulttimeout(120)
    
    url = 'http://%s%s' % (host, path)

    try:
        # get correct proxy variable
        if not PROXIES[0]: current_proxy = {}
        else: current_proxy = {'http': PROXIES[0]} # {'http':'http://some-proxy:port/'}

        if use_proxies:
            # install current proxy into opener                
            opener = urllib2.build_opener(urllib2.ProxyHandler(current_proxy))
        else:
            # no proxy
            opener = urllib2.build_opener()

        urllib2.install_opener(opener)

        content = opener.open(url, data).read()
        return content

    except Exception, e:
        print u'ERROR in method _post!', e
        return None




def getTop10(query):
    RESULTS = []

    # register IP
    # print urllib.urlopen('http://xml.yandex.ru/ip_set.xml?ip=94.73.248.137&submit=%C7%E0%EF%EE%EC%ED%E8%F2%FC+%3E').read()

    request = """<?xml version='1.0' encoding='utf-8'?>
            <request>
                <query>%s</query>
                <page>0</page>
                <maxpassages>0</maxpassages>
                <groupings>
                <groupby attr='d' mode='deep' groups-on-page='10' docs-in-group='1' curcateg='-1' />
                </groupings>
            </request>""" % str( escape(query.encode('utf8')) )
            
            
    
    current_request_failed = True
    while current_request_failed:
        # get server response
        xml = _post('xmlsearch.yandex.ru', '/xmlsearch', request, 'text/plain')

        soup = BeautifulSoup(xml)

        # Яндекс вернул ошибку?
        error = soup.find('error')
        if error:
            print u'YANDEX ERROR! %s' % error.contents[0]
            random.shuffle(PROXIES)
            continue
                    
        # this iteration was successful - so, set error flag to False
        current_request_failed = False


    soup_results = soup.findAll('domain')
    for soup_result in soup_results:
        domain = soup_result.contents[0]
        RESULTS.append(domain)

    return RESULTS
    
    
    
def getGoogleTop(query, region, n_top=10):
    """
    uses excellent xgoogle library
    taken from here: http://www.catonmat.net/blog/python-library-for-google-search/
    """
    result_urls = []

    query = query.encode('utf8')

    gs = GoogleSearch(query, region)
    gs.results_per_page = n_top
    results = gs.get_results()

    for idx, res in enumerate(results):
      parsed = urlparse(res.url)
      result_urls.append(res.url)

    return result_urls
    
    

def getYandexRelevantPage(query, for_site):
    """
    возвращает страницу сайта, наиболее релевантную запросу
    """
    
    request = """<?xml version='1.0' encoding='utf-8'?>
            <request>
                <query>%s site:%s</query>
                <page>0</page>
                <maxpassages>0</maxpassages>
                <groupings>
                <groupby attr='d' mode='deep' groups-on-page='10' docs-in-group='1' curcateg='-1' />
                </groupings>
            </request>""" % (query, for_site)
            
    xml = _post('xmlsearch.yandex.ru', YANDEX_PATH, request, 'text/plain', False)
    
    if not xml: return None
    
    xml = xml.replace('<_PassagesType>0</_PassagesType>','')

    soup = BeautifulSoup(xml)

    # Яндекс вернул ошибку?
    error = soup.find('error')
    if error:
        print u'YANDEX ERROR! %s' % error.contents[0]
        return None

    soup_result = soup.find('url')
    if soup_result: return soup_result.contents[0]    
    
    
    
def getGoogleRelevantPage(query, for_site):
    gs = GoogleSearch('%s %s' % (query, for_site))
    gs.results_per_page = 1
    results = gs.get_results()
    
    if not results: return None

    res = results[0]
    return res.url


def getYandexPosition(query, max_pages=5, region=None):
    global PROXIES
    
    items_per_page = 100
    request = """<?xml version='1.0' encoding='utf-8'?>
        <request>
            <query>%s</query>
            <page>{{ PAGE }}</page>
            <maxpassages>0</maxpassages>
            <groupings>
            <groupby attr='d' mode='deep' groups-on-page='%i' docs-in-group='1' curcateg='-1' />
            </groupings>
        </request>""" % ( str( escape(query.query).encode('utf8') ), items_per_page )
    page = 0
    result = False
    while page < max_pages:
        # add our parameters
        request2 = request
        request2 = request2.replace('{{ PAGE }}', str( page ) )

        current_request_failed = True
        while current_request_failed:
            # get server response
            if region: response = _post('xmlsearch.yandex.ru', '/xmlsearch?lr=%i' % region, request2, 'text/plain')
            else: response = _post('xmlsearch.yandex.ru', '/xmlsearch', request2, 'text/plain')
    
            if not response:
                print('error getting response - check your internet connection')
                random.shuffle(PROXIES)
                continue
    
            # parse response
            xml = ElementTree.fromstring(response)
    
            iterator = xml.getiterator()
    
            # check for errors
            for element in iterator:
                if element.tag == 'error':
                    if element.text:
                        print "ERROR: " + element.text
                        random.shuffle(PROXIES)
                        continue
                    
            # this iteration was successful - so, set error flag to False
            current_request_failed = False

        # find site position
        position = page*items_per_page
        for element in iterator:
            if element.tag == 'url': # if we find url node...
                if element.text:
                    position += 1
                    serp_domain = str(element.text).lower().strip().replace('http://www.', '').replace('www.', '').replace('http://', '')
                    #print serp_domain, query.site.host.lower()
                    if serp_domain.startswith( query.site.host.lower() ):
                        # if host is found...
                        return position
        page += 1

    return 999 # if site is not found


def getGooglePosition(target_domain, query, region):
    """
    uses excellent xgoogle library
    taken from here: http://www.catonmat.net/blog/python-library-for-google-search/
    """
    def mk_nice_domain(domain):
        """
        convert domain into a nicer one (eg. www3.google.com into google.com)
        """
        domain = re.sub("^www(\d+)?\.", "", domain)
        # add more here
        return domain

    query = query.encode('utf8')

    gs = GoogleSearch(query, region)
    gs.results_per_page = 100
    results = gs.get_results()

    for idx, res in enumerate(results):
      parsed = urlparse(res.url)
      domain = mk_nice_domain(parsed.netloc)
      if domain == target_domain:
        return idx+1

    return 999


def getGoogleIndexed(host):
    host = host.replace('http://','')
    gs = GoogleSearch('site:%s' % host)
    return gs.num_results



def getPR(host):
    pr = GooglePagerank(host)
    try:
        result = pr.get_pagerank()
    except CheckError, e:
        print '%s' % e
        return False
    except IpError, e:
        print '%s' % e
        return False
    return result.pr



def getYandexIndexed(url):
    # register IP
    # print urllib.urlopen('http://xml.yandex.ru/ip_set.xml?ip=94.73.248.137&submit=%C7%E0%EF%EE%EC%ED%E8%F2%FC+%3E').read()
    
    global PROXIES

    if url.startswith('http://'): url = url.replace('http://','')
    domain_parts = url.split('.')
    domain_parts.reverse()
    url = '.'.join(domain_parts)

    request = """<?xml version='1.0' encoding='utf-8'?>
            <request>
                <query>rhost="%s*"|rhost="%s.www*"</query>
                <page>0</page>
                <maxpassages>0</maxpassages>
                <groupings>
                <groupby attr='d' mode='deep' groups-on-page='10' docs-in-group='1' curcateg='-1' />
                </groupings>
            </request>""" % (url, url)
            
            
    current_request_failed = True
    while current_request_failed:
        # get server response
        response = _post('xmlsearch.yandex.ru', '/xmlsearch', request, 'text/plain')
    
        if not response:
            print('error getting response - check your internet connection')
            random.shuffle(PROXIES)
            continue
    
        # parse response
        soup = BeautifulSoup(response)
    
        # Яндекс вернул ошибку?
        error = soup.find('error')
        if error:
            if error.contents[0] == u'Искомая комбинация слов нигде не встречается': return 0
            else:
                print u'YANDEX ERROR! %s' % error.contents[0]
                random.shuffle(PROXIES)
                continue
                    
        # this iteration was successful - so, set error flag to False
        current_request_failed = False

    soup_result = soup.find('doccount')
    if not soup_result:
        soup_result = soup.find('found', {'priority':'all',})
        if not soup_result: return None
        return soup_result.contents[0]

    return soup_result.contents[0]



def getWordstatPopularity(query, region=''):
    class MyOpener(urllib.FancyURLopener):
        global BROWSERS
        #random.shuffle(BROWSERS)
        agent = BROWSERS[0]
        version = agent # custom user-agent
        proxies={}

    wordstat_url = u'http://wordstat.yandex.ru/?cmd=words&page=1&text=%s&geo=&text_geo=' % urllib.quote(query)
    myopener = MyOpener()
    try:
        content = myopener.open(wordstat_url).read()
    except Exception, e:
        print e
        return False
    content = content.replace('&nbsp;', ' ').replace('<br />','').replace('\n','')
    if 'Введите число,<br>указанное на картинке' in content:
        print 'ERROR: wordstat wants captcha!'
        return False
    matches = re.search(r'Что искали (?:.+)?( \d+ )'.decode('utf8'), content, re.U) # "Что искали со словами «интернет магазин к...» — 5285 показа в месяц"
    print matches

    #soup = BeautifulSoup(content)
    #p = soup.find( 'p', { 'class': 'smaller', } )
    #print p
    
    
    
def getNumberOfBacklinks(domain):
    YAHOO_REQUEST = "http://search.yahooapis.com/SiteExplorerService/V1/inlinkData?appid=%s&query=http://%s&results=1&omit_inlinks=domain" % (YAHOO_APPID, domain)
    try:
        src = urllib.urlopen( YAHOO_REQUEST, proxies={} ).read()
    except Exception, e:
        print u"Ошибка обработки запроса"
        return None
    
    soup = BeautifulSoup(src)

    # проверяем, если Яху вернул ошибку
    error = soup.find('error')
    if error:
        print u'YAHOO ERROR! %s' % error.message.contents[0]
        return None

    # узнаем число бэклинков
    result_set = soup.find('resultset')
    if not result_set:
        print u'ResultSet not found!'
        return None

    return int(result_set['totalresultsavailable'])
    
    
    
def getRawYahooBacks(domain):
        results = []

        start_iter = 1
        end_iter = 2

        while start_iter <= end_iter:
            # make the request
            YAHOO_REQUEST = "http://search.yahooapis.com/SiteExplorerService/V1/inlinkData?appid=%s&query=http://#DOMAIN#&results=100&start=#START#&omit_inlinks=domain" % YAHOO_APPID
            
            yahoo_request = YAHOO_REQUEST.replace('#DOMAIN#', domain).replace( '#START#', str(start_iter) )
            try:
                src = urllib.urlopen( yahoo_request, proxies={} ).read()
            except Exception, e:
                print u"Ошибка обработки запроса"
                print e
                sys.exit()

            soup = BeautifulSoup(src)

            # проверяем, если Яху вернул ошибку
            error = soup.find('error')
            if error:
                print u'YAHOO ERROR! %s' % error.message.contents[0]
                time.sleep(1)
                continue

            # ставим количество итераций
            result_set = soup.find('resultset')
            if not result_set:
                print u'ResultSet not found!'
                time.sleep(1)
                continue

                print u'Количество бэклинков по Yahoo для домена %s: %i' % ( domain, int(result_set['totalresultsavailable']) )
                end_iter = int(result_set['totalresultsavailable']) / 100 + 1

            soup_results = soup.findAll('result')

            for soup_result in soup_results:
                url = soup_result.url.contents[0] # URL страницы, на которой находится ссылка
                results.append( url )

            start_iter += 1

        return results
    



def getExistingYahooBacks(domain, internal_links, skip_num_links):
        results = []

        start_iter = 1
        end_iter = 2

        while start_iter <= end_iter:
            # make the request
            YAHOO_REQUEST = "http://search.yahooapis.com/SiteExplorerService/V1/inlinkData?appid=%s&query=http://#DOMAIN#&results=100&start=#START#&omit_inlinks=domain" % YAHOO_APPID
            
            yahoo_request = YAHOO_REQUEST.replace('#DOMAIN#', domain).replace( '#START#', str(start_iter) )
            try:
                src = urllib.urlopen( yahoo_request, proxies={} ).read()
            except Exception, e:
                print u"Ошибка обработки запроса"
                print e
                sys.exit()

            soup = BeautifulSoup(src)

            # проверяем, если Яху вернул ошибку
            error = soup.find('error')
            if error:
                print u'YAHOO ERROR! %s' % error.message.contents[0]
                time.sleep(1)
                continue

            # ставим количество итераций
            result_set = soup.find('resultset')
            if not result_set:
                print u'ResultSet not found!'
                time.sleep(1)
                continue

            print u'Количество бэклинков по Yahoo для домена %s: %i' % ( domain, int(result_set['totalresultsavailable']) )
            end_iter = int(result_set['totalresultsavailable']) / 100 + 1

            soup_results = soup.findAll('result')

            for soup_result in soup_results:
                url = soup_result.url.contents[0] # URL страницы, на которой находится ссылка
                clickurl = soup_result.clickurl.contents[0] # фактическая ссылка, которой ссылкается на наш домен страница
                """
                if options.straight: # чистим непрямые ссылки
                    print urlparse( str(clickurl) ).netloc, url
                    if urlparse( str(clickurl) ).netloc != options.domain: continue
                    """
                backlink_text = getBackLinkAnchor( str(url), domain ) # получаем анкор бэклинка
                link_exists = 1
                if not backlink_text:
                    if backlink_text == False: # URL не содержит ссылки вообще
                        link_exists = 0
                        print u'URL %s на самом деле не содержит ссылки на домен %s' % ( str(url), domain )
                    elif backlink_text.strip() == u'': # анкор ссылки - пустой
                        link_exists = 1
                        print u'URL %s содержит ссылку на домен %s с пустым анкором' % ( str(url), domain )

                results.append( (url, link_exists, backlink_text) )

            start_iter += 1

        return results



def getTIC(url):
            """
            определение ТИЦ для ссылки (по методу сравнения картинки от Яндекс.Денежки)
            взято отсюда: http://www.zarublem.su/pedia/61.html
            """
            dirname = os.path.join( os.path.dirname(__file__), 'cy_images' )
            cy_files = os.listdir(dirname)
            try:
                src = urllib.urlopen( 'http://yandex.ru/cycounter?%s' % url, proxies={} ).read()
            except:
                return False
            for cy_file in cy_files:
                fH = open(os.path.join(dirname,cy_file), 'r')
                file_content = fH.read()
                fH.close()
                if src == file_content: return cy_file.lower().replace('.gif','')
            return False


def getBackLinkAnchor(url, target_link):
            try:
                src = urllib.urlopen( url, proxies={} ).read()
            except:
                return False
            try:
                soup = BeautifulSoup(src)
            except:
                return False
            links = soup.findAll('a', href=re.compile(target_link))
            if not len(links): return False
            else:
                try:
                    link_anchor = unicode( cp1251_utf8(links[0].contents[0]) )
                    return link_anchor
                except:
                    return u''


def checkIfQueryIsInAnchor(query, anchor):
    STOPWORDS = (
        u'в', u'а', u'и', u'о', u'на', u'с', u'без', u'к', u'но', u'под', u'из', u'от',
    )
    query = query.lower()
    anchor = anchor.lower()
    for query_word in query.split(' '):
        query_word = query_word.strip()
        if query_word not in STOPWORDS and query_word not in anchor: return False
    return True


def getActualPrice(current_tic, prices_list):
	for tic, price in prices_list:
		if int(current_tic) == int(tic): return int(price)
	# if actual price can not be determined
	for i in range( len(prices_list)-1 ):
		if i == len(prices_list)-1: # если последняя итерация - возвращаем последнюю цену
			return int( prices_list[len(prices_list)][1] )
		tic1, price1 = prices_list[i]
		tic2, price2 = prices_list[i+1]
		if int(current_tic) > int(tic1) and int(current_tic) < int(tic2):
			return int( (price1+price2)/2 )
