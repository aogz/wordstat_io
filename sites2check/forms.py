#-*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django import forms
from sites2check.models import *
from regions.models import *
import datetime

from include.xlwt.Style import colour_map


class SiteForm(forms.ModelForm):
    def clean(self):
        self.cleaned_data['host'] = self.cleaned_data.get('host','').strip()
        return self.cleaned_data

    class Meta:
        model = Site2Check
        fields = ('host',)
        
        

class SiteBulkForm(forms.Form):
    sites = forms.CharField(label='Сайты', help_text='Список сайтов, по одному на каждую строку',
                            widget=forms.Textarea)
    
    def clean_sites(self):
        sites = self.cleaned_data.get('sites','')
        if len( sites.split('\n') ) > 10000:
            raise forms.ValidationError(u'Слишком большое число сайтов')
        return sites



class QueryForm(forms.ModelForm):
    region_str = forms.CharField(label='Регион по Яндексу', required=False, help_text='Введите первые цифры региона (оставьте пустым, если запрос не имеет геотаргетинга)')
    
    def __init__(self,**kwrds):
        """
        подставляем название региона в поле
        """
        super(QueryForm,self).__init__(**kwrds)
        if 'instance' in kwrds:
            if kwrds['instance'].region:
                self.fields['region_str'].initial = kwrds['instance'].region.title_ru
            else:
                self.fields['region_str'].initial = ''
    
    def clean_hilite_color(self):
		color = self.cleaned_data['hilite_color'].strip()
		if color and color not in colour_map.keys():
				valid_colors = ''
				for valid_color in colour_map.keys():
						valid_colors += '%s, ' % valid_color
				raise forms.ValidationError(u'Недопустимое название цвета. Допустимые: %s' % valid_colors)
		return color
        
    def clean(self):
        region_str = self.cleaned_data.get('region_str','').strip()
        if region_str:
            try: region = Region.objects.get(title_ru=region_str)
            except: raise forms.ValidationError(u'Неправильный регион')
        self.cleaned_data['query'] = self.cleaned_data.get('query','').strip()
        return self.cleaned_data

    class Meta:
        model = Query
        fields = ('query', 'region_str', 'google_region', 'hilite_color',)



class EditQueryBulkForm(forms.Form):
    queries = forms.CharField(label='Запросы', help_text='По одному запросу на каждую строку', widget=forms.Textarea)


class DatesForm(forms.Form):
        date_from = forms.DateField(('%d/%m/%Y',), label='От', initial=datetime.datetime.now()-datetime.timedelta(days=1), widget=forms.DateTimeInput(format='%d/%m/%Y', attrs={'class':'input','readonly':'readonly','size':'7'}))
        date_to = forms.DateField(('%d/%m/%Y',), label='До', initial=datetime.datetime.now(), widget=forms.DateTimeInput(format='%d/%m/%Y', attrs={'class':'input','readonly':'readonly','size':'7'}))