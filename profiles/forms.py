#-*- coding: utf-8 -*-

from django import forms
from captcha.fields import CaptchaField
from billing.models import *

import time

class AddFundsForm(forms.Form):
    code1 = forms.CharField(label='Код 1')
    code2 = forms.CharField(label='Код 2')
    captcha = CaptchaField(label='Защитный код')