#-*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin

# from profiles.signals import *
from sites2check.models import *
# from billing.models import *



# User profile
class UserProfile(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь', unique=True)
    balance = models.FloatField('Баланс, руб.', default=5)

    def get_absolute_url(self):
        return u'/profiles/%s/' % self.user.username

    class Meta:
        verbose_name = "Профиль пользователя"
        verbose_name_plural = "Профили пользователей"

    def get_user_sites(self):
        return Site2Check.objects.filter(user=self.user)

    def get_user_queries(self):
        return Query.objects.filter(site__user=self.user)

    def get_billing_per_day(self):
        user_queries = Query.objects.filter(site__user=self.user)
        billing_settings = BillingSettings.objects.all()[0]
        total_user_month_billing = user_queries.count() * billing_settings.cost_per_query
        return total_user_month_billing / 30
    get_billing_per_day.short_description = 'Расход в день'
    get_billing_per_day.allow_tags = True


    def get_balance_remaining_days(self):
        user_queries = Query.objects.filter(site__user=self.user)
        billing_settings = BillingSettings.objects.all()[0]
        total_user_month_billing = user_queries.count() * billing_settings.cost_per_query
        total_user_day_billing = total_user_month_billing / 30
        user_balance = self.user.get_profile().balance
        if user_balance <= 0: return 0
        if total_user_day_billing == 0: return '∞'
        return int(user_balance / total_user_day_billing)
    get_balance_remaining_days.short_description = 'Осталось дней'
    get_balance_remaining_days.allow_tags = True


    def decrease_user_balance_dayly(self):
        day_billing = self.get_billing_per_day()
        if self.balance > 0:
            self.balance -= day_billing
            self.save()
            return day_billing
        return 0