from wordstat import settings

from simple_registration.signals import user_created


def user_profile_create(sender, **kwargs):
    """
    this called every time a new user has created his account
    """
    from profiles.models import *
    if 'user' in kwargs:
        user = kwargs['user']
        up, is_created = UserProfile.objects.get_or_create(user=user)
user_created.connect(user_profile_create)
