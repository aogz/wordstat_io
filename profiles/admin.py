from django.contrib import admin

from profiles.models import *

class UserProfileAdmin(admin.ModelAdmin):
	list_display = ('user', 'balance', 'get_billing_per_day', 'get_balance_remaining_days')
	search_fields = ('user__username',)
admin.site.register(UserProfile, UserProfileAdmin)
