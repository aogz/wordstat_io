#-*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404

from django.contrib.auth.decorators import login_required

from profiles.models import *
from profiles.forms import *

from decorators import *

from django.contrib import messages

import datetime, md5



@url(r'^$') # /profiles/
@render_to('profile.html')
@login_required
def profile(request):
	user_profile = request.user.get_profile()
	return locals()


@url(r'^add-funds/$') # /profiles/add-funds/
@render_to('add_funds.html')
@login_required
def add_funds(request):
	""" add funds to user account """
	if request.method == 'POST':
		form = AddFundsForm(data=request.POST)
		if form.is_valid():
			code1 = form.cleaned_data.get('code1', '')
			code2 = form.cleaned_data.get('code2', '')
			try:
				card = PrepayedCard.objects.get(code1=code1, code2=code2, is_used=False, used_by=None)
			except:
				time.sleep(3)
				messages.error(request, u'Введены неверные коды. Проверьте правильность введенных кодов.')
				return {'form':form,}
			
			card.is_used = True
			card.used_by = request.user
			card.save()
			
			user_profile = request.user.get_profile()
			user_profile.balance += card.value
			user_profile.save()
			
			messages.info(request, u'Карта на сумму %i рублей успешно активирована' % card.value)

			return {'form':form,}
		else:
			return {'form':form,}
	else:
		form = AddFundsForm()
		return {'form':form,}


@url(r'^_create/$') # /profiles/_create/
@login_required
def create_profiles(request):
	# creates profiles for users who don't have it
	if not request.user.is_superuser:
		return HttpResponse('you can not call this url - become superuser first')
	users = User.objects.all()
	users_processed = 0
	for user in users:
		up, is_created = UserProfile.objects.get_or_create(user=user)
		users_processed += 1
	
	return HttpResponse('done. %i users processed' % users_processed)