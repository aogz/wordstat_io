import dateutil.parser
import json, django
from sites2check.models import Query, Position

f = open('sites2check/fixtures/positions.json', 'r')
obj = json.load(f)

for o in obj:
    p = Position.objects.create(query_id=o['fields']['query_pk'])
    p.query_id=o['fields']['query_pk']
    p.position=o['fields']['position']
    p.searchEngine=o['fields']['searchEngine']
    p.created=dateutil.parser.parse(o['fields']['created'])
    p.save() 
