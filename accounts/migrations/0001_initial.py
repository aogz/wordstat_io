# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Account'
        db.create_table(u'accounts_account', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('price', self.gf('django.db.models.fields.FloatField')(default=0, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('limit_per_10_hours', self.gf('django.db.models.fields.IntegerField')()),
            ('limit_per_10_hours_api', self.gf('django.db.models.fields.IntegerField')()),
            ('limit_simultaneous_xls_exports', self.gf('django.db.models.fields.IntegerField')(default=5)),
            ('export_max_pages', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'accounts', ['Account'])

        # Adding model 'UserProfile'
        db.create_table(u'accounts_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('typ', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_profile', unique=True, to=orm['auth.User'])),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='profiles', null=True, to=orm['accounts.Account'])),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='children', null=True, to=orm['accounts.UserProfile'])),
            ('api_token', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=100, null=True, blank=True)),
            ('api_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('referral_redirect_url', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
        ))
        db.send_create_signal(u'accounts', ['UserProfile'])

        # Adding model 'Invite'
        db.create_table(u'accounts_invite', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='invites_from', unique=True, to=orm['auth.User'])),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('when_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'accounts', ['Invite'])

        # Adding model 'Payment'
        db.create_table(u'accounts_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_payments', to=orm['auth.User'])),
            ('for_account', self.gf('django.db.models.fields.related.ForeignKey')(related_name='account_payments', to=orm['accounts.Account'])),
            ('when_initialed', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('when_finished', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('amount', self.gf('django.db.models.fields.FloatField')()),
            ('is_success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_test', self.gf('django.db.models.fields.BooleanField')()),
            ('info', self.gf('django.db.models.fields.TextField')(max_length=10000, null=True, blank=True)),
        ))
        db.send_create_signal(u'accounts', ['Payment'])

        # Adding model 'PromoCode'
        db.create_table(u'accounts_promocode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=50, unique=True, null=True, blank=True)),
            ('activated', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('activated_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='user_coupons', null=True, to=orm['auth.User'])),
            ('when_activated', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('activated_for_payment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.Payment'], null=True, blank=True)),
            ('discount', self.gf('django.db.models.fields.FloatField')()),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'accounts', ['PromoCode'])


    def backwards(self, orm):
        # Deleting model 'Account'
        db.delete_table(u'accounts_account')

        # Deleting model 'UserProfile'
        db.delete_table(u'accounts_userprofile')

        # Deleting model 'Invite'
        db.delete_table(u'accounts_invite')

        # Deleting model 'Payment'
        db.delete_table(u'accounts_payment')

        # Deleting model 'PromoCode'
        db.delete_table(u'accounts_promocode')


    models = {
        u'accounts.account': {
            'Meta': {'ordering': "('-weight',)", 'object_name': 'Account'},
            'export_max_pages': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limit_per_10_hours': ('django.db.models.fields.IntegerField', [], {}),
            'limit_per_10_hours_api': ('django.db.models.fields.IntegerField', [], {}),
            'limit_simultaneous_xls_exports': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        u'accounts.invite': {
            'Meta': {'object_name': 'Invite'},
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invites_from'", 'unique': 'True', 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'when_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'accounts.payment': {
            'Meta': {'object_name': 'Payment'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'for_account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'account_payments'", 'to': u"orm['accounts.Account']"}),
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_payments'", 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'null': 'True', 'blank': 'True'}),
            'is_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_test': ('django.db.models.fields.BooleanField', [], {}),
            'when_finished': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'when_initialed': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'accounts.promocode': {
            'Meta': {'ordering': "['-activated']", 'object_name': 'PromoCode'},
            'activated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'activated_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'user_coupons'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'activated_for_payment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Payment']", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'discount': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'when_activated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'accounts.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'profiles'", 'null': 'True', 'to': u"orm['accounts.Account']"}),
            'api_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'api_token': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['accounts.UserProfile']"}),
            'referral_redirect_url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'typ': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_profile'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['accounts']