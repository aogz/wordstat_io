import hashlib
import datetime

#random string
import string
import random
##############

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

# from helpers.utils import random_string

from django.conf import settings


def random_string(length):
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for _ in range(length))


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class Account(models.Model):
    """ Tariff - like 'light', 'premium', 'gold' etc. """
    name = models.CharField(_('Name'), max_length=100, unique=True)
    price = models.FloatField(_('Price'), default=0, blank=True, null=True,
                              help_text='USD\month')
    weight = models.IntegerField(
        unique=True,
        help_text=_('Less-expensive gets less values - so, bronze is 1, '
                    'silver is 2, gold is 3'))
    limit_per_10_hours = models.IntegerField(
        help_text=_('Number of allowed requests per 10 hours'))
    limit_per_10_hours_api = models.IntegerField(
        help_text=_('Number of allowed API requests per 10 hours'))
    limit_simultaneous_xls_exports = models.IntegerField(
        help_text=_('Number of allowed simultaneous XLS exports'),
        default=5
    )
    limit_for_keywords_monitoring = models.IntegerField(help_text=_('Number of allowed keywords '
                                                                    'for site monitoring'))
    export_max_pages = models.IntegerField(
        _('How many first pages to export to XLS or view in full report'),
        help_text=_('Maximum number of pages to export'),
        blank=True, null=True
    )

    def num_of_users_on_account(self):
        return self.profiles.count()

    def __unicode__(self):
        return u'Account %s' % self.name

    class Meta:
        ordering = ('-weight', )


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class UserProfile(models.Model):
    """ Django's custom user profile """
    typ_choices = (
        ('agency', _('an internet-marketing agency')),
        ('website_owner', _('a website owner or team'))
    )
    typ = models.CharField(blank=True, null=True, max_length=255)
    user = models.ForeignKey(User, unique=True, related_name='user_profile')
    account = models.ForeignKey(Account, related_name='profiles',
                                blank=True, null=True)
    parent = models.ForeignKey(
        'self', related_name='children', blank=True, null=True,
        help_text=_('User who invited you'))  # for MLM
    api_token = models.CharField(_('API token'), max_length=100, blank=True,
                                 null=True, db_index=True)

    api_enabled = models.BooleanField(default=False)

    referral_redirect_url = models.CharField(
        _('Redirect URL'), blank=True, null=True, max_length=150,
        help_text=_('Where your referrals should be redirected (see below for more info)'),
    )

    def num_of_children(self):
        return self.children.count()

    def generate_api_token(self):
        if not self.api_token:
            self.api_token = hashlib.sha1(
                'SDds _n JdsKSD32 sdfdsf  34 sdf  ss3 DS nNd 3f' + str(self.pk)
                + str(self.user.username.encode('ascii', errors='ignore'))
            ).hexdigest()
            self.save()

    def when_account_expires(self):
        """ Returns the date when user's paid account expires """
        active_payments = self.user.user_payments.filter(
            is_success=True).order_by('-when_finished')
        if not active_payments:
            return
        last_payment = active_payments[0]
        if not last_payment.when_finished:
            return
        return last_payment.when_finished + datetime.timedelta(days=30)

    def is_expired(self):
        """ Returns True if the user's account was paid in the past but now
             expired
        """
        when_expires = self.when_account_expires()
        if when_expires is None:
            return
        try:
            _exp = bool(datetime.datetime.now() > when_expires)
        except Exception, e:
            if "can't compare offset-naive" in str(e):
                _exp = bool(timezone.now() > when_expires)
            else:
                assert False, str(e)
        return _exp

    def is_api_enabled(self):
        if self.account.price != 0:
            return True  # API enabled for all paid plans
        else:
            return self.api_enabled  # otherwise check user's flag

    def __unicode__(self):
        if not self.api_token:
            self.generate_api_token()
        return self.user.username


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class Invite(models.Model):
    from_user = models.ForeignKey(User, related_name='invites_from',
                                  unique=True)
    token = models.CharField(max_length=255, blank=True, null=True)
    when_created = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(
        default=True,
        help_text='Set this to False to disable the invite'
    )

    def __unicode__(self):
        return _('Invite from %s') % self.from_user

    def save(self, *args, **kwargs):
        if not self.token:  # generate token automatically
            self.token = random_string(length=24)
            # generate really unique token
            while Invite.objects.filter(token=self.token).count():
                self.token = random_string(length=24)
        super(Invite, self).save(*args, **kwargs)

    def accepted_invites(self):
        """ Returns number of registered users ('accepted invites') """
        return UserProfile.objects.filter(
            parent=self.from_user, user__is_active=True
        ).count()


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def _create_profile(user):
    basic_account = Account.objects.all().order_by('weight')
    if not len(basic_account):
        assert False, 'no accounts found!'
    basic_account = basic_account[0]
    profile, is_created = UserProfile.objects.get_or_create(user=user)
    if not profile.account:
        profile.account = basic_account
        profile.save()
    return profile
User.profile = property(_create_profile)


def _create_refcode(user):
    """ Create a unique refcode for the user """
    invite, is_created = Invite.objects.get_or_create(from_user=user)
    return invite.token
User.refcode = property(_create_refcode)


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class Payment(models.Model):
    from_user = models.ForeignKey(User, related_name='user_payments')
    for_account = models.ForeignKey(Account, related_name='account_payments')

    when_initialed = models.DateTimeField(auto_now_add=True)
    when_finished = models.DateTimeField(blank=True, null=True)

    amount = models.FloatField()  # in USD

    is_success = models.BooleanField(default=False)

    is_test = models.BooleanField()  # see ROBOKASSA_TEST_MODE

    info = models.TextField(blank=True, null=True, max_length=10000)

    def __unicode__(self):
        return "Payment <%s> from <%s>" % (self.pk, self.from_user.username)


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class PromoCode(models.Model):
    """ Discount promotions (coupons). Each code can be activated only 1 time.
    """
    code = models.CharField(max_length=50, blank=True, null=True, unique=True)
    activated = models.BooleanField(default=False)
    activated_by = models.ForeignKey(User, related_name='user_coupons',
                                     blank=True, null=True)
    when_activated = models.DateTimeField(blank=True, null=True)
    activated_for_payment = models.ForeignKey(Payment, blank=True, null=True)
    discount = models.FloatField(help_text='Discount, in percents')
    note = models.TextField(blank=True, null=True,
                            help_text='Any information that might be useful')

    def save(self, *args, **kwargs):
        # generate random code if it's empty
        if not self.code:
            new_code = random_string(length=20)
            while 1:
                new_code = random_string(length=20)
                if PromoCode.objects.filter(code=new_code).count():
                    continue
                break
            self.code = new_code
        return super(PromoCode, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-activated']

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class PromoCodeAccountUpgrade(models.Model):
    code = models.CharField(max_length=50, blank=True, null=True, unique=True)
    activated = models.BooleanField(default=False)
    activated_by = models.ForeignKey(User, related_name='user_code_for_upgrade',
                                     blank=True, null=True)
    when_activated = models.DateTimeField(blank=True, null=True)
    target_account = models.ForeignKey(Account, related_name="promo_code_for_upgrade", blank=True,
    									null=True)
    duration = models.IntegerField(help_text='duration of a promo code in days')

    def __unicode__(self):
    	return self.code

    def save(self, *args, **kwargs):
        # generate random code if it's empty
        if not self.code:
            new_code = random_string(length=25)
            while 1:
                new_code = random_string(length=25)
                if PromoCodeAccountUpgrade.objects.filter(code=new_code).count():
                    continue
                break
            self.code = new_code
        return super(PromoCodeAccountUpgrade, self).save(*args, **kwargs)

    def expired(self, *args, **kwargs):
    	if self.activated:
    		if self.when_activated + datetime.timedelta(days=self.duration) < datetime.datetime.now():
    			return True
    		else:
    			return False

    class Meta:
        ordering = ['-when_activated']

# from .signals import *