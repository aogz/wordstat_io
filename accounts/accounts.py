from django.shortcuts import render, HttpResponse
from .models import PromoCodeAccountUpgrade, UserProfile, Account
import datetime


class CheckCookies(object):

	def process_request(self, request):
		try:
			if request.user.is_authenticated() and request.COOKIES['promo_code']:
				promo_code = PromoCodeAccountUpgrade.objects.get(code=request.COOKIES['promo_code'])
				promo_code.activated_by = request.user
				promo_code.activated = True
				promo_code.when_activated = datetime.datetime.now()
				promo_code.save()

				user = UserProfile.objects.get(user=request.user)
				user.account = promo_code.target_account
				user.save()

				response = HttpResponse('Your promo code has been activated!')
				response.delete_cookie('promo_code')

				return response
		except:
			pass

class CheckAccountStatus(object):

	def process_request(self, request):
		try:
			promo_code = PromoCodeAccountUpgrade.objects.filter(activated_by=request.user)
			if promo_code[0].expired():
				user = UserProfile.objects.get(user=request.user)
				user.account = Account.objects.get(name='Free')
				user.save()
		except:
			pass


