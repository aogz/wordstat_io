from django.test import TestCase
from models import PromoCodeAccountUpgrade, Account, UserProfile
from django.contrib.auth.models import User
import datetime
# Create your tests here.

class PromoCodeAccountUpgradeTest(TestCase):

	def setUp(self):
		self.user = User.objects.create(username='testuser')
		self.gold_account = Account.objects.create(name='Gold', weight=3, limit_per_10_hours=3,
							limit_per_10_hours_api=5, limit_simultaneous_xls_exports=4)
		self.silver_account = Account.objects.create(name='Silver', weight=2, limit_per_10_hours=3,
							limit_per_10_hours_api=5, limit_simultaneous_xls_exports=4)

		self.promo_code = PromoCodeAccountUpgrade.objects.create(target_account=self.silver_account, 
							activated_by=self.user, when_activated=datetime.datetime.now()-datetime.timedelta(days=1),
							activated=True, duration=5)

		
	def expired_promo_code(self):
		promo_code_expired = PromoCodeAccountUpgrade.objects.create(target_account=self.gold_account, 
							activated_by=self.user, 
							when_activated=datetime.datetime.now()-datetime.timedelta(days=6),
							activated=True, duration=5)
		return promo_code_expired


	def test_model_creation(self):
		self.assertFalse(self.promo_code.expired())
		self.assertTrue(self.expired_promo_code().expired())
		self.assertEqual(PromoCodeAccountUpgrade.objects.filter(
			activated_by=self.user)[0].code, self.promo_code.code)
