from django.conf.urls import patterns, include, url
import views


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wordstat.views.home', name='home'),
    url(r'^expired/$', views.ListofExpiredView.as_view(), name='list_of_expired'),
    url(r'^promo-code/(?P<promo_code>\w+)/$', views.PromoCodeUpgradeAccountView.as_view(), name='promo_code_account_upgrade'),
    url(r'^generate-promo-code/$', views.generate_promo_code, name='generate_promo_code'),
)