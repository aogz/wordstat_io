from django.shortcuts import render, redirect
from django.db.models import Q
from django.http import HttpResponse
from django.views.generic import View
from .models import UserProfile, Account, Payment, PromoCodeAccountUpgrade

import datetime

# Create your views here.

class ListofExpiredView(View):

	def get(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return HttpResponse('non-authenticated')
		if not request.user.is_superuser and not request.user.is_staff:
			return HttpResponse('you must be a superuser or staff')

		now_plus = datetime.datetime.now() + datetime.timedelta(days=14)
		now_minus = datetime.datetime.now() - datetime.timedelta(days=14)

		user_list = [profile for profile in UserProfile.objects.filter(account__price__gt=0)
	    			if profile.when_account_expires() and profile.when_account_expires() >= 
	    			now_minus and profile.when_account_expires() <= now_plus 
	    			]

		return render(request, 'users_expire.html', locals())


class PromoCodeUpgradeAccountView(View):

	def get(self, request, promo_code=None, *args, **kwargs):
		try:
			promo_code = PromoCodeAccountUpgrade.objects.get(code=promo_code)
			if promo_code.activated:
				return HttpResponse("Your promo code is already in use")
		except:
			return HttpResponse("Your promo code is not valid")

		if request.user.is_authenticated():
			promo_code.activated_by = request.user
			promo_code.activated = True
			promo_code.when_activated = datetime.datetime.now()
			promo_code.save()

			user = UserProfile.objects.get(user=request.user)
			user.account = promo_code.target_account
			user.save()

			print promo_code.code
			print user.account
		else:
			response = redirect('/sign-up/')
			response.set_cookie('promo_code', promo_code.code)
			return response
		return HttpResponse("Your Promo Code Succesfully activated. Now Your account has a Gold status.")


# For testing
def generate_promo_code(request):
	promo_code = PromoCodeAccountUpgrade.objects.create()
	promo_code.save()

	return HttpResponse(promo_code.code)

def sign_up_test_view(request):
	try:
		return HttpResponse(request.COOKIES['promo_code'])
	except:
		return HttpResponse('No cookies.')
#