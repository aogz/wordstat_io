from django.contrib import admin
from .models import *

# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):
	pass

class PaymentAdmin(admin.ModelAdmin):
	pass

class AccountAdmin(admin.ModelAdmin):
	pass

class PromoCodeAccountUpgradeAdmin(admin.ModelAdmin):
	pass

admin.site.register(PromoCodeAccountUpgrade, PromoCodeAccountUpgradeAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Account, AccountAdmin)