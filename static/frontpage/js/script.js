/* Author:
	Paul Bremer 2012 (@paul_bremer)
*/
$('.carousel').carousel({
  interval: 20000
});

/* Screenshots w/ PrettyPhoto */
$("a[rel^='prettyPhoto']").prettyPhoto({
	show_title: false,
	theme: 'pp_default',
	social_tools: false
});

$('#frm-user').validate();

/*
 * Modal box submit action
 */

/*
$('#btn-md-user-submit').click(function(){
    if($('#frm-user').valid() && !$(this).hasClass('disabled')) {
        $('#frm-user').submit();
    }
});
$('#frm-user').submit(function(){
    $('#btn-md-user-submit').addClass('disabled');
    $.post('/submit/', $('#frm-user').serialize(), function(response){
        $('#frm-md-user-response').append(response).show();
    }).complete(function(){
        $('#md-user .close').trigger('click');
        $('#frm-user')[0].reset();
        $('#btn-md-user-submit').removeClass('disabled');
    });
    return false;
});

*/