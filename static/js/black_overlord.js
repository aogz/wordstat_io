function setCookie(cname,cvalue,expire)
{
    var d = new Date();
    d.setTime(d.getTime()+expire);
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++)
        {
            var c = ca[i].trim();
            if (c.indexOf(name)==0) return c.substring(name.length,c.length);
        }
    return "";
}


(function ($) {
    // TODO! the function is not usable at the moment =(
    $.fn.blacken = function() {
        $(this).submit(function(event) {
            // check if the user has submitted the form already (by cookie),
            //  and don't display the BO to him
            if (getCookie("_search_form_submitted")) {
                return this;
            }

            setCookie(_search_form_submitted, '1', 1000*4)

            $('body').prepend("<div class='black_overlord'>&nbsp;</div>");
            $('body').prepend("<div class='ajax_loader_global'><img src='/static/img/loader2.gif'/></div>");
            event.preventDefault();
            //$('body').delay(5000);
            $(this).submit();

        });
    };

}(jQuery));