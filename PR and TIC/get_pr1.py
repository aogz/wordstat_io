# -*- coding: utf-8 -*-

import re
import urllib
import urllib2
import httplib


# Settings
prhost='toolbarqueries.google.com'
prpath='/tbr?client=navclient-auto&ch=%s&features=Rank&q=info:%s'


def get_yandex_tic(URL):
        if "http://" not in URL:
            URL = "http://{URL}".format(URL=URL)
            
        yandex_url = 'http://bar-navig.yandex.ru/u?ver=2&show=32&url={URL}'.format(URL=urllib.quote(URL))
        try:
            page = urllib2.urlopen(yandex_url, timeout=7).read()
        except Exception, error:
            return "CONNECTION PROBLEMS"
        try:
            TIC = re.findall(r'\<tcy rang\=\".*?\" value\=\"(\d+)\"\/\>', page)[0]
        except IndexError:
            TIC = 0
        return str(TIC).decode("UTF-8")




# Function definitions
def GetHash (URL):
    SEED = "Mining PageRank is AGAINST GOOGLE'S TERMS OF SERVICE. Yes, I'm talking to you, scammer."
    Result = 0x01020345
    for i in range(len(URL)) :
        Result ^= ord(SEED[i%len(SEED)]) ^ ord(URL[i])
        Result = Result >> 23 | Result << 9
        Result &= 0xffffffff
    return '8%x' % Result
 

def get_google_pr (URL):
    conn = httplib.HTTPConnection(prhost)
    hash = GetHash(URL)
    path = prpath % (hash,URL)
    conn.request("GET", path)
    response = conn.getresponse()
    data = response.read()
    conn.close()
    if data == '':
        return '0'
    return data.split(":")[-1]

